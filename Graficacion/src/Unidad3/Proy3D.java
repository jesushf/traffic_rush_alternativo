package Unidad3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;
import java.net.URL;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Unidad2.cuadroDialogo;

class manejar2 extends MouseMotionAdapter{
	Proy3D obj;
	public manejar2(Proy3D T) {
		obj=T;
	}
	public void mouseDragged(MouseEvent e) {
		Point posa=e.getPoint();
		if(obj.mover){
			obj.objf.Trasladar((int)(posa.x-obj.objf.fig2d[1][0]),
					(int)(posa.y-obj.objf.fig2d[1][1]));
			obj.tx.setValue(0);
			obj.ty.setValue(0);
		}
		obj.repaint(); 
		obj.var=true;
	}
}
class manejar extends MouseAdapter{
	Proy3D obj;
	public manejar(Proy3D T) {
		obj=T;
	}
	public void mousePressed(MouseEvent e) {
		Point pres=e.getPoint();
	    if(obj.objf.checar(pres))
		    obj.mover=true;
		else
			obj.mover=false;
	   // System.out.println("dentro: "+obj.mover);
	    /*if(!obj.mover)
	    obj.rotar=!(obj.rotar);
	    if(obj.rotar)
	    {
	    	obj.objf.rotarSentidoE(5);
	    	obj.repaint();
	    }*/
	}
}
public class Proy3D extends JPanel implements MouseWheelListener{
	JFrame Vent;
	fig3D objf;
	boolean mover=false,var=false;
	JSlider s1x,s2y,s3z,s4d,tx,ty;
	int antx=0,anty=0,trasx,trasy;
	JMenuBar barraM;
	double fig2d[][];
	Image imagen;
	public Proy3D(String tit, int sec[],double fig[][]){
		Vent=new JFrame(tit);
		Vent.setSize(1000,700);
		barraM=new JMenuBar ();
		ponerMenu();
		Vent.setJMenuBar(barraM);
		fig2d=new double[fig.length][fig[0].length];
		for(int i=0;i<fig.length;i++)
			for(int j=0;j<fig[0].length;j++)
				fig2d[i][j]=fig[i][j];
		URL ruta=getClass().getResource("/Unidad3/recursos/pared2.jpg/");
		Toolkit tk=Toolkit.getDefaultToolkit();
		imagen=tk.getImage(ruta);
		//Agregar el panel
		objf=new fig3D(fig,sec,1000);
		Vent.add(this,BorderLayout.CENTER);
		//crear sliders y agregarlos
		
		JPanel s1T=new JPanel();
		TitledBorder tit1=new TitledBorder("Rotacion en x");
		tit1.setTitleJustification(TitledBorder.CENTER);
		s1T.setBorder(tit1);
		s1x=new JSlider(JSlider.HORIZONTAL,0,360,0);
		s1x.setMajorTickSpacing(50);
		s1x.setPaintTicks(true);
		s1x.setPaintLabels(true);
		s1x.setMinorTickSpacing(5);
		s1T.add(s1x);
		JPanel s2T=new JPanel();
		TitledBorder tit2=new TitledBorder("Rotacion en y");
		tit2.setTitleJustification(TitledBorder.CENTER);
		s2T.setBorder(tit2);
		s2y=new JSlider(JSlider.HORIZONTAL,0,360,0);
		s2y.setMajorTickSpacing(50);
		s2y.setPaintTicks(true);
		s2y.setPaintLabels(true);
		s2y.setMinorTickSpacing(5);
		s2T.add(s2y);
		JPanel s3T=new JPanel();
		TitledBorder tit3=new TitledBorder("Rotacion en z");
		tit3.setTitleJustification(TitledBorder.CENTER);
		s3T.setBorder(tit3);
		s3z=new JSlider(JSlider.HORIZONTAL,0,360,0);
		s3z.setMajorTickSpacing(50);
		s3z.setPaintTicks(true);
		s3z.setPaintLabels(true);
		s3z.setMinorTickSpacing(5);
		s3T.add(s3z);
		
		s4d=new JSlider(JSlider.VERTICAL,100,2000,1000);
		s4d.setMajorTickSpacing(250);
		s4d.setPaintTicks(true);
		s4d.setPaintLabels(true);
		s4d.setMinorTickSpacing(50);
		JPanel trax=new JPanel();
		TitledBorder tit5=new TitledBorder("Traslacion en x");
		tit5.setTitleJustification(TitledBorder.CENTER);
		trax.setBorder(tit5);
		tx=new JSlider(JSlider.HORIZONTAL,0,Vent.getWidth()-200,0);
		tx.setMajorTickSpacing(100);
		tx.setPaintTicks(true);
		tx.setPaintLabels(true);
		tx.setMinorTickSpacing(5);
		trax.add(tx);
		JPanel tray=new JPanel();
		TitledBorder tit6=new TitledBorder("Traslacion en Y");
		tit6.setTitleJustification(TitledBorder.CENTER);
		tray.setBorder(tit6);
		ty=new JSlider(JSlider.HORIZONTAL,0,Vent.getHeight()-300,0);
		ty.setMajorTickSpacing(100);
		ty.setPaintTicks(true);
		ty.setPaintLabels(true);
		ty.setMinorTickSpacing(5);
		tray.add(ty);
		Vent.addMouseWheelListener(this);
		addMouseListener(new manejar(this));
		addMouseMotionListener(new manejar2(this));
		s4d.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				objf.cambiarDist(s4d.getValue());
				tx.setValue(0);
				ty.setValue(0);
				repaint();
			}
			
		});
		Vent.add(s4d,BorderLayout.EAST);
		JPanel gs=new JPanel();
		gs.setLayout(new FlowLayout());
		gs.add(s1T);gs.add(s2T);gs.add(s3T);
		JPanel gt=new JPanel();
		gt.setLayout(new FlowLayout());
		gt.add(trax);gt.add(tray);
		s2y.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent d)
			{
				int angx=s1x.getValue();
				int angy=s2y.getValue();
				int angz=s3z.getValue();
				objf.rotar(angx,angy,angz);
				tx.setValue(0);
				ty.setValue(0);
				repaint();
			}
		});
		s1x.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent d)
			{int angx=s1x.getValue();
			int angy=s2y.getValue();
			int angz=s3z.getValue();
			objf.rotar(angx,angy,angz);
			tx.setValue(0);
			ty.setValue(0);
			repaint();
			}
		});
		s3z.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent d)
			{
				int angx=s1x.getValue();
				int angy=s2y.getValue();
				int angz=s3z.getValue();
				objf.rotar(angx,angy,angz);
				tx.setValue(0);
				ty.setValue(0);
				repaint();
			}
		});
		tx.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent g)
			{
				var=true;
				trasx=tx.getValue();
				trasy=ty.getValue();
				if(antx>=trasx)
				{
					trasx=antx-trasx;
					trasx*=-1;
				}
				if(anty>=trasy)
				{
					trasy=anty-trasy;
					trasy*=-1;
				}
				if(antx<trasx)
					trasx-=antx;
				if(anty<trasy)
					trasy-=anty;
				//trasy*=-1;
				objf.Trasladar(trasx,trasy);
				antx=tx.getValue();
				anty=ty.getValue();
				repaint();
			}
		});
		ty.addChangeListener(new ChangeListener(){
			public void stateChanged(ChangeEvent f)
			{
				var=true;
				trasx=tx.getValue();
				trasy=ty.getValue();
				if(antx>=trasx)
				{
					trasx=antx-trasx;
					trasx*=-1;
				}
				if(anty>=trasy)
				{
					trasy=anty-trasy;
					trasy*=-1;
				}
				if(antx<trasx)
					trasx-=antx;
				if(anty<trasy)
					trasy-=anty;
				//trasy*=-1;
				objf.Trasladar(trasx,trasy);
				antx=tx.getValue();
				anty=ty.getValue();
				repaint();
			}
		});
		Vent.add(gs,BorderLayout.NORTH);
		Vent.add(gt,BorderLayout.SOUTH);
		Vent.setVisible(true);
		Vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(imagen, 0, 0, this.getSize().width, this.getSize().height,this);
		if(!var)
			objf.proyeccionPersp();
		g.setColor(Color.BLACK);
		g.drawString("Escalera",(int)objf.fig2d[24][0],(int)objf.fig2d[24][1]);
		objf.dibujar2D(g);
		objf.vistaLateral(g);
		objf.vistaSuperior(g);
		objf.vistaFrontal(g);
		var=false;
	}
	public static void main(String []x)
	{
		double escalera[][]={{-24,60,-16},//0A
				{-16,60,-16},//1B
				{16,60,-16},//2C
				{24,60,-16},//3D
				{-24,-60,-16},//4E
				{-16,-60,-16},//5F
				{16,-60,-16},//6G
				{24,-60,-16},//7H
				{-16,36,-16},//8I
				{16,36,-16},//9J
				{-16,28,-16},//10K
				{16,28,-16},//11L
				{-16,12,-16},//12M
				{16,12,-16},//13N
				{-16,4,-16},//14�
				{16,4,-16},//15O
				{-16,-12,-16},//16P
				{16,-12,-16},//17Q
				{-16,-20,-16},//18R
				{16,-20,-16},//19S
				{-16,-36,-16},//20T
				{16,-36,-16},//21U
				{-16,-44,-16},//22V
				{16,-44,-16},//23W
				//atras
				//,,,,,,,,,,,,,,,,,,,,,,,,
				{-24,60,16},//0A'
				{-16,60,16},//1B'
				{16,60,16},//2C'
				{24,60,16},//3D'
				{-24,-60,16},//4E'
				{-16,-60,16},//5F'
				{16,-60,16},//6G'
				{24,-60,16},//7H'
				{-16,36,16},//8I'
				{16,36,16},//9J'
				{-16,28,16},//10K'
				{16,28,16},//11L'
				{-16,12,16},//12M'
				{16,12,16},//13N'
				{-16,4,16},//14�'
				{16,4,16},//15O'
				{-16,-12,16},//16P'
				{16,-12,16},//17Q'
				{-16,-20,16},//18R'
				{16,-20,16},//19S'
				{-16,-36,16},//20T'
				{16,-36,16},//21U'
				{-16,-44,16},//22V'
				{16,-44,16},//23W'
		};
		int secescalera[]={9,11,11,10,10,8,8,12,12,13,13,15,15,14,14,12,12,16,16,17,17,19,19,18,18,16,16,20,20,21,21,23,23,22,22,20,20,5,5,4,4,0,0,1,1,8,8,9,9,2,2,3,3,7,7,6,6,2,//ENFRENTE
				33,35,35,34,34,32,32,36,36,37,37,39,39,38,38,36,36,40,40,41,41,43,43,42,42,40,40,44,44,45,45,47,47,46,46,44,44,29,29,28,28,24,24,25,25,32,32,33,33,26,26,27,27,31,31,30,30,26,//ATRAS
				0,24,1,25,2,26,3,27,4,28,5,29,6,30,7,31,8,32,9,33,10,34,11,35,12,36,13,37,14,38,15,39,16,40,17,41,18,42,19,43,20,44,21,45,22,46,23,47};//UNIR EL EJE Z
		new Proy3D("Transformaciones en 3D",secescalera,escalera);
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int notches=e.getWheelRotation();
		if(notches<0)
		{//arriba
			int valorBarra=s4d.getValue();
			valorBarra+=5;
			if(valorBarra>2000)
				valorBarra=2000;
			objf.cambiarDist(valorBarra);
			s4d.setValue(valorBarra);
		}
		else
		{//abajo
			int valorBarra=s4d.getValue();
			valorBarra-=5;
			if(valorBarra<1)
				valorBarra=1;
			objf.cambiarDist(valorBarra);
			s4d.setValue(valorBarra);
		}
	}
	public void ponerMenu()
	{
		JMenu Transformar,Opciones;
		Transformar =new JMenu("Transformar");
		Opciones=new JMenu("Transformaciones");
		barraM.add(Transformar);
		//barraM.add(Opciones);
		JMenuItem op1,op2,op3,op7;
		URL ruta=getClass().getResource("/Unidad3/recursos/restaurar.jpg");
		Action ac1=new AbstractAction("Restaurar",new ImageIcon(ruta)){
			public void actionPerformed(ActionEvent e)
			{
				restaurar();
				repaint();
			}
	};
	ac1.putValue(Action.MNEMONIC_KEY, new Integer('R'));
	ac1.putValue(Action.SHORT_DESCRIPTION, "Restaura la figura");
	ac1.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R,InputEvent.CTRL_MASK));
		op1=new JMenuItem("Restaurar");
		op1.setAction(ac1);
		Transformar.add(op1);
		URL ruta2=getClass().getResource("/Unidad3/recursos/zoomm.jpg");
		Action ac2=new AbstractAction("Cambiar tama�o",new ImageIcon(ruta2)){
			public void actionPerformed(ActionEvent e)
			{
				String fs=JOptionPane.showInputDialog("Dame la cantidad de tama�o a cambiar ");
				int Rx;
				try{
			    	Rx=Integer.parseInt(fs);
			    }catch(NumberFormatException x){
			    	Rx=s4d.getValue();
			    }
				objf.cambiarDist(Rx);
				tx.setValue(0);
				ty.setValue(0);
				s4d.setValue(Rx);
				repaint();
			}
	};
	
	ac2.putValue(Action.MNEMONIC_KEY, new Integer('E'));
	ac2.putValue(Action.SHORT_DESCRIPTION, "Escalar la figura");
	ac2.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
		op2=new JMenuItem("Escalar");
		op2.setAction(ac2);
		Transformar.add(op2);
		
		URL ruta3=getClass().getResource("/Unidad3/recursos/rotard.jpg");
		Action ac3=new AbstractAction("Rotar",new ImageIcon(ruta3)){
			public void actionPerformed(ActionEvent e)
			{
				rotar obj=new rotar(Vent,true);
				if(obj.valores[0]==0)
				{
					s1x.setValue(obj.valores[1]);
				}
					
				else
					if(obj.valores[0]==1)
						s2y.setValue(obj.valores[1]);
					else
						if(obj.valores[0]==2)
							s3z.setValue(obj.valores[1]);
				objf.rotar(s1x.getValue(),s2y.getValue(),s3z.getValue());
				tx.setValue(0);
				ty.setValue(0);
				repaint();
			}
	};
	
	ac3.putValue(Action.MNEMONIC_KEY, new Integer('Y'));
	ac3.putValue(Action.SHORT_DESCRIPTION, "Rotar la figura");
	ac3.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_Y,InputEvent.CTRL_MASK));
		op3=new JMenuItem("Rotar");
		op3.setAction(ac3);
		Transformar.add(op3);
		
		URL ruta7=getClass().getResource("/Unidad3/recursos/trasladar.jpg");
		Action ac7=new AbstractAction("Trasladar",new ImageIcon(ruta7)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en Y");
					    int Tx=0,Ty=0;
					    try{
					    	Tx=Integer.parseInt(fsx);
					    	Ty=Integer.parseInt(fsy);
					    }catch(NumberFormatException x){
					    	Tx=Ty=0;
					    }
					    tx.setValue(Tx);
					    ty.setValue(Ty);
					    objf.Trasladar(Tx,Ty);
					    var=true;
					    repaint();
			}
	};
	
	ac7.putValue(Action.MNEMONIC_KEY, new Integer('T'));
	ac7.putValue(Action.SHORT_DESCRIPTION, "Trasladar la figura");
	ac7.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_T,InputEvent.CTRL_MASK));
		op7=new JMenuItem("Trasladar");
		op7.setAction(ac7);
		Transformar.add(op7);
	}
	public void restaurar()
	{
		for(int i=0;i<objf.fig2d.length;i++)
			for(int j=0;j<objf.fig2d[0].length;j++)
				objf.fig2d[i][j]=fig2d[i][j];
		s1x.setValue(0);
		s2y.setValue(0);
		s3z.setValue(0);
		objf.cambiarDist(1000);
		s4d.setValue(1000);
		tx.setValue(0);
		ty.setValue(0);
		var=false;
	}
}
