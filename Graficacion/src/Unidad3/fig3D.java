package Unidad3;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class fig3D {
	double fig3d[][],orig[][],fig2d[][],fig2dv[][];
	double distancia,mz=-350;
	private Graphics g;
	int secF[];
	double maxx,maxy,minx,miny;
	public fig3D(double figt[][],int sec[], int d)
	{
		secF=sec;
		fig3d=new double[figt.length][figt[0].length];
		orig=new double[figt.length][figt[0].length];
		fig2d=new double[figt.length][2];
		fig2dv=new double[figt.length][2];
		for(int i=0;i<figt.length;i++)
			for(int j=0;j<figt[0].length;j++)
			{
				fig3d[i][j]=figt[i][j];
				orig[i][j]=figt[i][j];
			}
		distancia=d;
	}
	public void proyeccionPersp(){
		for(int i=0;i<fig3d.length;i++)
		{
			fig2d[i][0]=distancia*-fig3d[i][0]/(fig3d[i][2]+mz);
			fig2d[i][1]=distancia*fig3d[i][1]/(fig3d[i][2]+mz);
			fig2d[i][0]+=100;
			fig2d[i][1]+=200;
		}
	}
	public void dibujar2D(Graphics g)
	{
		g.setColor(Color.BLUE);
		for(int i=0;i<secF.length-1;i+=2)
			g.drawLine((int)fig2d[secF[i]][0], (int)fig2d[secF[i]][1], (int)fig2d[secF[i+1]][0], (int)fig2d[secF[i+1]][1]);
	}
	
	
	public void rotacionZ(int grados)
	{
		double angR=Math.toRadians(grados);
		double sen=Math.sin(angR);
		double cos=Math.cos(angR);
		for(int i=0;i<fig3d.length;i++)
		{
			double x=fig3d[i][0];
			double y=fig3d[i][1];
			double z=fig3d[i][2];
			fig3d[i][0]=x*cos-y+sen;
			fig3d[i][1]=x*sen+y*cos;
			fig3d[i][2]=z;
		}
	}
	public  void rotar(int angx,int angy,int angz)
	{
		double radx=Math.toRadians(angx);
		double rady=Math.toRadians(angy);
		double radz=Math.toRadians(angz);
		double caa=Math.cos(radx);
		double saa=Math.sin(radx);
		double cab=Math.cos(rady);
		double sab=Math.sin(rady);
		double cac=Math.cos(radz);
		double sac=Math.sin(radz);
		for(int i=0;i<fig3d.length;i++)
		{
			double x=orig[i][0];
			double y=orig[i][1];
			double z=orig[i][2];
			fig3d[i][0]=x*(cab*cac)+y*((saa*-sab)*cac+(caa*sac))+z*((caa*-sab)*cac+(-saa*-sac));
			fig3d[i][1]=x*(cab*sac)+y*((saa*-sab)*sac+(caa*cac))+z*((caa*-sab)*sac-(saa*cac));
			fig3d[i][2]=x*sab+y*(saa*cab)+z*(caa*cab);
		}	
	}
	public void cambiarDist(int dist)
	{
		distancia=dist;
	}
	public void Trasladar(int Tx, int Ty){
		for(int i=0;i<fig2d.length;i++){
			fig2d[i][0]+=Tx;
			fig2d[i][1]+=Ty;
			//fig3d[i][0]+=Tx;
			//fig3d[i][1]+=Ty;
			//System.out.println("si");
		}
}
	public boolean checar(Point t){
		//minx y miny
		minx=fig2d[0][0];
		miny=fig2d[0][1];
		for(int i=1;i<fig2d.length;i++){
			if(fig2d[i][0]<minx)
				minx=fig2d[i][0];
			if(fig2d[i][1]<miny)
				miny=fig2d[i][1];
				}
		maxx=fig2d[0][0];
		maxy=fig2d[0][1];
		for(int i=1;i<fig2d.length;i++){
			if(fig2d[i][0]>maxx)
				maxx=fig2d[i][0];
			if(fig2d[i][1]>maxy)
				maxy=fig2d[i][1];
				}
		if(t.x>minx && t.y>miny && t.x<maxx && t.y<maxy)
			return true;
		else
			return false;
	}
	public void vistaSuperior(Graphics g)
	{
		double figVS[][]=rotacionX(90);//rotar 90 grados
		//eliminar la profundidad
		for(int i=0;i<figVS.length;i++)
		{
			figVS[i][2]=0;
		}
		//dibujarla
		proyeccionPersp1(figVS,550,200);
		dibujarVistas(g,"Vista Superior");
	}
	public void vistaLateral(Graphics g)
	{
		double figVS[][]=rotacionY(90);//rotar 90 grados
		//eliminar la profundidad
		for(int i=0;i<figVS.length;i++)
		{
			figVS[i][2]=0;
		}
		//dibujarla
		proyeccionPersp1(figVS,300,200);
		g.setColor(Color.BLACK);
		g.drawString("Vista lateral",(int)fig2dv[24][0],(int)fig2dv[24][1]);
		g.setColor(Color.BLUE);
		dibujarVistas(g,"Vista Lateral");
	}
	public void vistaFrontal(Graphics g)
	{
		double figVS[][]=new double[fig3d.length][fig3d[0].length];//rotar 90 grados
		//eliminar la profundidad
		for(int i=0;i<figVS.length;i++)
		{
			figVS[i][0]=fig3d[i][0];
			figVS[i][1]=fig3d[i][1];
			figVS[i][2]=0;
		}
		//dibujarla
		proyeccionPersp1(figVS,800,200);
		dibujarVistas(g,"Vista Frontal");
	}
	public void dibujarVistas(Graphics g, String ca)
	{
		g.setColor(Color.BLACK);
		if(!ca.equals("Vista Lateral"))
		g.drawString(ca, (int)(fig2dv[0][0]), (int)(fig2dv[0][1]));
		g.setColor(Color.BLUE);
		for(int i=0;i<secF.length-1;i+=2)
			g.drawLine((int)fig2dv[secF[i]][0], (int)fig2dv[secF[i]][1], (int)fig2dv[secF[i+1]][0], (int)fig2dv[secF[i+1]][1]);
	}
	public double[][] rotacionX(int grados)
	{
		double temp[][]=new double[fig3d.length][fig3d[0].length];
		double angR=Math.toRadians(grados);
		double sen=Math.sin(angR);
		double cos=Math.cos(angR);
		for(int i=0;i<fig3d.length;i++)
		{
			double x=fig3d[i][0];
			double y=fig3d[i][1];
			double z=fig3d[i][2];
			temp[i][0]=x;
			temp[i][1]=y*cos-z*sen;
			temp[i][2]=y*sen+z*cos;
		}
		return temp;
	}
	public double[][] rotacionY(int grados)
	{
		double temp[][]=new double[fig3d.length][fig3d[0].length];
		double angR=Math.toRadians(grados);
		double sen=Math.sin(angR);
		double cos=Math.cos(angR);
		for(int i=0;i<fig3d.length;i++)
		{
			double x=fig3d[i][0];
			double y=fig3d[i][1];
			double z=fig3d[i][2];
			temp[i][0]=x*cos-z*sen;
			temp[i][1]=y;
			temp[i][2]=x*sen+z*cos;
		}
		return temp;
	}
	public void proyeccionPersp1(double fig3[][], int x,int y){
		for(int i=0;i<fig3.length;i++)
		{
			fig2dv[i][0]=distancia*-fig3[i][0]/(fig3[i][2]+mz);
			fig2dv[i][1]=distancia*fig3[i][1]/(fig3[i][2]+mz);
			fig2dv[i][0]+=x;
			fig2dv[i][1]+=y;
		}
	}
}
