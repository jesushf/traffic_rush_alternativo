package Unidad3;
import java.awt.FlowLayout;
import java.net.URL;
import java.awt.event.*;
import javax.swing.*;
//cuadro de dialogo para una rotacion
public class rotar extends JDialog{
	JLabel et1,et2,et3,et4;
	JRadioButton rd1,rd2,rd3;
	ButtonGroup gb;
	JTextField ct1;
	JButton b1,b2;
	int valores[];
	public rotar(JFrame v,boolean modal){
		super(v,modal);
		valores=new int[2];
		setTitle("Selecciona el eje de rotacion y asigna el angulo");
		setSize(600,150);
		setLocation(50,500);
		setLayout(new FlowLayout());
		URL ruta=getClass().getResource("/Unidad3/recursos/x.png");
		et1=new JLabel(new ImageIcon(ruta));
		ruta=getClass().getResource("/Unidad3/recursos/y.jpg");
		et2=new JLabel(new ImageIcon(ruta));
		ruta=getClass().getResource("/Unidad3/recursos/z.png");
		et4=new JLabel(new ImageIcon(ruta));
		gb=new ButtonGroup();
		rd1=new JRadioButton();
		rd2=new JRadioButton();
		rd3=new JRadioButton();
		gb.add(rd1);
		gb.add(rd2);
		gb.add(rd3);
		et3=new JLabel("grados:");
		ct1=new JTextField(5);
		b1=new JButton("Aceptar");
		b2=new JButton("Cancelar");
		add(et1);add(rd1);
		add(et2);add(rd2);
		add(et4);add(rd3);
		add(et3);add(ct1);
		add(b1);add(b2);
		//setVisible(true);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent f){
				if(rd1.isSelected())
					valores[0]=0;
				else
					if(rd2.isSelected())
						valores[0]=1;
					else
						if(rd3.isSelected())
							valores[0]=2;
				String valor=ct1.getText();
				//System.out.println(valor);
				try{
					valores[1]=Integer.parseInt(valor);
				}
			catch(NumberFormatException d)
			{
				valores[1]=0;
			}
				setVisible(false);
			}
		});
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent d){
			valores[0]=0;
			valores[1]=0;
			setVisible(false);}
		});
		setVisible(true);
	}
	public int [] retornar()
	{
		int va[]={0,0};
		if(valores[1]!=0)
			return valores;
		else
			return va;					
	}
}
