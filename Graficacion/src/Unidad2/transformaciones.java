package Unidad2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.net.URL;

import javax.swing.*;
class manejadora1 extends MouseAdapter{
	transformaciones obj;
	public manejadora1(transformaciones T) {
		obj=T;
	}
	public void mousePressed(MouseEvent e) {
		Point pres=e.getPoint();
	    if(obj.fig.checar(pres))
		    obj.mover=true;
		else
			obj.mover=false;
	    System.out.println("dentro: "+obj.mover);
	}
}
class manejadora2 extends MouseMotionAdapter{
	transformaciones obj;
	public manejadora2(transformaciones T) {
		obj=T;
	}
	public void mouseDragged(MouseEvent e) {
		Point posa=e.getPoint();
		if(obj.mover){
			obj.fig.Trasladar((int)(posa.x-obj.fig.fig2[1][0]),
					(int)(posa.y-obj.fig.fig2[1][1]));
		}
		obj.repaint();
	}
}
public class transformaciones extends JPanel implements ActionListener, MouseWheelListener{
	JFrame Vent;
	figura fig;
	JButton besc,brotc,brots,bdef,btras,brest,bref;
	boolean mover=false;
	JToolBar barrah;
	JMenuBar barraM;
	JButton brd,bri,be1,be2;
	int opr=0,	ang=5;
	public void barraherr()
	{
		URL rutaImg=getClass().getResource("/Unidad2/recursos/rotard.jpg");
		   brd=new JButton();
		   brd.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/rotari.jpg");
		   bri=new JButton();
		   bri.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/escala1.png");
		   be1=new JButton();
		   be1.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/escala2.png");
		   be2=new JButton();
		   be2.setIcon(new ImageIcon(rutaImg));
		   barrah=new JToolBar("Operacines Rapidas",JToolBar.VERTICAL);
		   barrah.setFloatable(false);
		   Vent.add(barrah,BorderLayout.EAST);
		   
		   barrah.add(brd);barrah.add(bri);barrah.add(be1);barrah.add(be2);
		   
		   brd.setToolTipText("Rotar 5 Grados a la derecha");
		   brd.setToolTipText("Rotar 5 Grados a la izquierda");
		   brd.setToolTipText("Escala la figura .1");
		   brd.setToolTipText("Disminuye la figura .1");
		   
		   brd.addActionListener(this);
		   bri.addActionListener(this);
		   be1.addActionListener(this);
		   be2.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==brd)
		{
			fig.rotarContraE(5);
		}
		else
			if(e.getSource()==bri)
			{
				fig.rotarSentidoE(5);
			}
			else
				if(e.getSource()==be1)
				{
					fig.EscalarE(1.1, 1.1);
				}
				else
					if(e.getSource()==be2)
					{
						fig.EscalarE(0.9, 0.9);
					}
		repaint();
	}
	public void ponerMenu()
	{
		JMenu Transformaciones,Opciones;
		Transformaciones =new JMenu("transformaciones");
		Opciones=new JMenu("Opciones");
		barraM.add(Transformaciones);
		barraM.add(Opciones);
		JMenuItem op1,op2,op3,op4,op5,op6;
		URL ruta=getClass().getResource("/Unidad2/recursos/trailerder.png");
		Action ac1=new AbstractAction("Restaurar",new ImageIcon(ruta)){
			public void actionPerformed(ActionEvent e)
			{
				fig.restaurar();
				repaint();
			}
	};
	ac1.putValue(Action.MNEMONIC_KEY, new Integer('R'));
	ac1.putValue(Action.SHORT_DESCRIPTION, "Restaura la figura");
	ac1.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R,InputEvent.CTRL_MASK));
		op1=new JMenuItem("Restaurar");
		op1.setAction(ac1);
		Transformaciones.add(op1);
		URL ruta2=getClass().getResource("/Unidad2/recursos/converder.png");
		Action ac2=new AbstractAction("Escalar",new ImageIcon(ruta2)){
			public void actionPerformed(ActionEvent e)
			{
				fig.EscalarE(1.1,1.1);
				repaint();
			}
	};
	
	ac2.putValue(Action.MNEMONIC_KEY, new Integer('E'));
	ac2.putValue(Action.SHORT_DESCRIPTION, "Escalar la figura");
	ac2.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
		op2=new JMenuItem("Escalar");
		op2.setAction(ac2);
		Transformaciones.add(op2);
		
		URL ruta3=getClass().getResource("/Unidad2/recursos/carroder.png");
		Action ac3=new AbstractAction("Rotar",new ImageIcon(ruta3)){
			public void actionPerformed(ActionEvent e)
			{
				cuadroDialogo obj=new cuadroDialogo(Vent,true);
				//obj.mostrarCuadro();
				if(obj.valores[0]==0)
				{
					fig.rotarSentidoE(obj.valores[1]);
					System.out.println(obj.valores[1]);
				}
				else
					fig.rotarContraE(obj.valores[1]);
			}
	};
	
	ac3.putValue(Action.MNEMONIC_KEY, new Integer('A'));
	ac3.putValue(Action.SHORT_DESCRIPTION, "Rotar la figura");
	ac3.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A,InputEvent.CTRL_MASK));
		op3=new JMenuItem("Rotar");
		op3.setAction(ac3);
		Transformaciones.add(op3);
	}
	public transformaciones(){
	   Vent=new JFrame("Transformaciones en 2D");	
	   Vent.setSize(800, 600);
	   barraherr();
	   fig=new figura();
	   Vent.add(this);
	   barraM=new JMenuBar ();
	   Vent.setJMenuBar(barraM);
	   ponerMenu();
	   besc=new JButton("Escalar");
	   brotc=new JButton("Rotar en Contra");
	   brots=new JButton("Rotar en el Sentido");
	   bdef=new JButton("Deformar");
	   btras=new JButton("Trasladar");
	   bref=new JButton("Reflexion");
	   brest=new JButton("Restaurar");
	   JPanel gb=new JPanel(); 
	   gb.setLayout(new FlowLayout());
	   gb.add(brest);
	   gb.add(besc);gb.add(brotc);gb.add(brots);
	   gb.add(bdef);gb.add(bref);gb.add(btras);
	   Vent.add(gb,BorderLayout.SOUTH);
	   brest.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			fig.restaurar();
			repaint();
		}});
	   brotc.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			String cang=JOptionPane.showInputDialog(
					"Dame en angulo de rotaci�n");
			int ang=0;
			try{
				ang=Integer.parseInt(cang);
			}catch(NumberFormatException ex){
			    ang=0;	
			}
			fig.rotarContraE(ang);
			repaint();
		}});
	   brots.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String cang=JOptionPane.showInputDialog(
				"Dame en angulo de rotaci�n");
				int ang=0;
				try{
					ang=Integer.parseInt(cang);
				}catch(NumberFormatException ex){
					ang=0;	
				}
				fig.rotarSentidoE(ang);
				repaint();
			}});
	   besc.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
				String fsx=JOptionPane.showInputDialog(
				  "Dame la cantidad a escalar en X");
				String fsy=JOptionPane.showInputDialog(
				  "Dame la cantidad a escalar en Y");
			    double sx=0.0,sy=0.0;
			    try{
			    	sx=Double.parseDouble(fsx);
			    	sy=Double.parseDouble(fsy);
			    }catch(NumberFormatException x){
			    	sx=sy=1.0;
			    }
			    fig.EscalarE(sx, sy);
			    repaint();
		}});
	   bdef.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					String fsx=JOptionPane.showInputDialog(
					  "Dame la cantidad a deformar en X");
					String fsy=JOptionPane.showInputDialog(
					  "Dame la cantidad a deformar en Y");
				    double dx=0.0,dy=0.0;
				    try{
				    	dx=Double.parseDouble(fsx);
				    	dy=Double.parseDouble(fsy);
				    }catch(NumberFormatException x){
				    	dx=dy=0.0;
				    }
				    fig.DeformarE(dx, dy);
				    repaint();
			}});
	   btras.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					String fsx=JOptionPane.showInputDialog(
					  "Dame la cantidad a Trasladar en X");
					String fsy=JOptionPane.showInputDialog(
					  "Dame la cantidad a trasladar en Y");
				    int Tx=0,Ty=0;
				    try{
				    	Tx=Integer.parseInt(fsx);
				    	Ty=Integer.parseInt(fsy);
				    }catch(NumberFormatException x){
				    	Tx=Ty=0;
				    }
				    fig.Trasladar(Tx, Ty);
				    repaint();
			}});
	   bref.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) 
		{	
			String fsx=JOptionPane.showInputDialog(
					  "Dame la cantidad a Trasladar en X");
					String fsy=JOptionPane.showInputDialog(
					  "Dame la cantidad a trasladar en Y");
				    int Rx=0,Ry=0;
				    try{
				    	Rx=Integer.parseInt(fsx);
				    	Ry=Integer.parseInt(fsy);
				    }catch(NumberFormatException x){
				    	Rx=Ry=0;
				    }
			fig.ReflexionE(Rx, Ry);
		}
	});
	   Vent.setVisible(true);
	   Vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   addMouseListener(new manejadora1(this));
	   addMouseMotionListener(new manejadora2(this));
	   addMouseWheelListener(this);
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g); //borra
		fig.dibujar2(g);
		g.drawRect(550, 400, 800, 600);
		fig.mapeo(800, 550, 800, 600, 400, 600);
		fig.dibujarM(g);
	}
	public static void main(String[] args) {
		new transformaciones();
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) 
	{
		int notches=e.getWheelRotation();
		if(notches>0)
		{
			fig.EscalarE(1.1,1.1);
		}
		else
			fig.EscalarE(0.9, 0.9);
		repaint();
	}
}
