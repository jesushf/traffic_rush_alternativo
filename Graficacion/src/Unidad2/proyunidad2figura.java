package Unidad2;

import java.awt.Graphics;
import java.awt.Point;

class puntoproy{
	int x,y;
	puntoproy(int a,int b){
		x=a; y=b;
	}
}
public class proyunidad2figura {
	int fig1[]={200,180,250,220,200,260,280,260,280,220,260,180,200,180};
	double fig2[][]={{133,80},{128,82},{123,83},{119,84},{115,87},{110,90},
			{106,94},{103,99},{102,104},{102,109},{104,113},{106,117},
			{110,120},{115,124},{121,128},{126,129},{131,130},{134,137},
			{134,143},{134,147},{135,152},{136,157},{137,164},{138,170},
			{140,174},{144,174},{147,169},{148,165},{149,160},{149,153},
			{150,147},{150,132},{157,134},{159,133},{165,135},{170,134},
			{176,135},{182,136},{187,137},{188,147},{184,148},{180,151},
			{176,154},{174,158},{174,163},{176,167},{181,171},{185,173},
			{189,175},{188,181},{188,186},{189,190},{189,194},{191,199},
			{195,194},{196,189},{230,189},{230,139},{248,138},{230,151},{230,151},
			{230,179},{277,137},{260,138},{248,138},{289,137},{299,135},{307,134},
			{314,134},{322,132},{330,131},{335,130},{339,128},{340,137},
			{342,142},{345,147},{348,152},{354,155},{359,154},{364,152},
			{368,148},{370,141},{373,136},{373,130},{372,77},{370,69},
			{368,65},{361,58},{357,59},{352,59},{347,61},{344,66},
			{341,73},{340,79},{339,83},{331,83},{327,82},{322,80},
			{317,78},{313,78},{308,77},{304,77},{299,76},{294,76},
			{290,76},{283,76},{275,75},{267,75},{261,75},{255,75},
			{249,75},{230,75},{276,75},{230,40},{230,60},{248,75},
			{260,75},{230,75},{230,30},{196,30},{196,25},{196,19},
			{195,15},{192,10},{189,15},{189,19},{189,23},{189,27},
			{189,32},{188,36},{189,40},{184,42},{178,45},{176,49},
			{174,53},{175,57},{178,62},{182,65},{186,66},{190,67},
			{190,76},{185,76},{179,77},{173,77},{169,78},{164,79},
			{160,79},{155,79},{151,80},{150,72},{150,66},{149,61},
			{149,57},{148,51},{148,43},{145,39},{141,38},{138,42},
			{136,47},{136,52},{136,59},{135,65},{135,69},{134,74},{133,80}};
	double figsec[][]={{110,100},{95,115},{105,110},{90,125},{100,120},
			{85,135},{95,130},{80,145},{90,140},{75,155},{85,150},
			{70,165},{80,160},{65,175},{75,170},{60,185},{70,180},
			{55,195},{65,190},{50,205},{60,200},{90,200},{110,185},
			{100,190},{115,175},{105,180},{120,165},{110,170},
			{125,155},{115,160},{130,145},{120,150},{135,135},{125,140},
			{140,125},{130,130},{145,115},{135,120},{150,105},{110,100}};
	puntoproy fig3[]={new puntoproy(200,180),new puntoproy(250,220),new puntoproy(200,260),new puntoproy(280,260),
			new puntoproy(280,220),new puntoproy(260,180),new puntoproy(200,180)};
	double original[][],figM[][],originalsec[][],figM2[][];
	double minx,miny,maxx,maxy;
	public proyunidad2figura(){
		original=new double[fig2.length][fig2[0].length];
		figM=new double[fig2.length][fig2[0].length];
		figM2=new double[figsec.length][figsec[0].length];
		for(int i=0;i<fig2.length;i++)
		{
			original[i][0]=fig2[i][0];
			original[i][1]=fig2[i][1];
		}
	}
	public void restaurar(int op)
	{
		if(op==1){
			fig2=new double[original.length][original[0].length];
		for(int i=0;i<fig2.length;i++){
			fig2[i][0]=original[i][0];
			fig2[i][1]=original[i][1];
		}}
		else
		figura(2);
	}
	public void figura(int op)
	{
		if(op==2){
			//System.out.println(op);
			fig2=new double[figsec.length][figsec[0].length];
			for(int i=0;i<fig2.length;i++)
			{
				fig2[i][0]=figsec[i][0];
				fig2[i][1]=figsec[i][1];
			}}
			if(op==1)
				restaurar(1);
	}
	void dibujar1 (Graphics g){
		for(int x=0,y=1;x<fig1.length-3;x+=2,y+=2){
			g.drawLine(fig1[x], fig1[y], fig1[x+2],fig1[y+2] );
		}
			
	}
	void dibujar2 (Graphics g){
		for(int r=0;r<fig2.length-1;r++){
			g.drawLine((int)fig2[r][0], (int)fig2[r][1], (int)fig2[r+1][0],(int)fig2[r+1][1]);
	}
	}
	void dibujarM (Graphics g, int op){
		if(op==1)
		for(int r=0;r<figM.length-1;r++)
		{
			g.drawLine((int)figM[r][0], (int)figM[r][1], (int)figM[r+1][0],(int)figM[r+1][1]);
			
		}else
		for(int r=0;r<figM2.length-1;r++)
		{
			g.drawLine((int)figM2[r][0], (int)figM2[r][1], (int)figM2[r+1][0],(int)figM2[r+1][1]);
			
		}
	}
	void dibujar3 (Graphics g){
		for(int r=0;r<fig3.length-1;r++)
			g.drawLine(fig3[r].x, fig3[r].y, fig3[r+1].x,fig3[r+1].y);
	}
	//transformaciones
		//[x*Sx,y*Sy]
		public void escalarorigen(double Sx, double Sy)
		{
			for(int i=0;i<fig2.length;i++)
			{
				fig2[i][0]=fig2[i][0]*Sx;
				fig2[i][1]=fig2[i][1]*Sy;
			}
		}
		//x*cos+y*sen,-x*sen+y*cos
		public void rotarSMorigen(int ang){
			//ang->radianes
			double angR=Math.toRadians(ang);
			double senAng=Math.sin(angR);
			double cosAng=Math.cos(angR);
			for(int i=0;i<fig2.length;i++)
			{
				double X=fig2[i][0];
				double Y=fig2[i][1];
				fig2[i][0]=X*cosAng+Y*senAng;
				fig2[i][1]=-X*senAng+Y*cosAng;
			}
		}
		//x*cos-y*sen,x*sen+y*cos
		public void rotarCMorigen(int ang){
			double angR=Math.toRadians(ang);
			double senAng=Math.sin(angR);
			double cosAng=Math.cos(angR);
			for(int i=0;i<fig2.length;i++)
			{
				double X=fig2[i][0];
				double Y=fig2[i][1];
				fig2[i][0]=X*cosAng-Y*senAng;
				fig2[i][1]=X*senAng+Y*cosAng;
			}
		}
		//Deformacion
		//[x+y*Dy,x*Dx+y]
		public void Deformacion(double Dx, double Dy){
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x+y*Dy;
				fig2[i][1]=x*Dx+y;
						
			}
		}
		//Trasladar
		//[x+Tx,y+Ty]
		public void Trasladar(int Tx, int Ty){
			for(int i=0;i<fig2.length;i++){
				fig2[i][0]+=Tx;
				fig2[i][1]+=Ty;
			}
		}
		public boolean checar(Point t){
			//minx y miny
			minx=fig2[0][0];
			miny=fig2[0][1];
			for(int i=1;i<fig2.length;i++){
				if(fig2[i][0]<minx)
					minx=fig2[i][0];
				if(fig2[i][1]<miny)
					miny=fig2[i][1];
					}
			maxx=fig2[0][0];
			maxy=fig2[0][1];
			for(int i=1;i<fig2.length;i++){
				if(fig2[i][0]>maxx)
					maxx=fig2[i][0];
				if(fig2[i][1]>maxy)
					maxy=fig2[i][1];
					}
			if(t.x>minx && t.y>miny && t.x<maxx && t.y<maxy)
				return true;
			else
				return false;
		}
//Transformaciones con respecto a un puntoproy
		public void rotarCMp(int ang){
			//paso 1 trasladar la figura al origen
			int Tx=(int)fig2[1][0];
			int Ty=(int)fig2[1][1];
			Trasladar(-Tx,-Ty);
			//paso 2 apllicarle la transformacion
			rotarCMorigen(ang);
			//paso 3 regresar la figura a la posicion original
			Trasladar(Tx,Ty);
		}
		public void Escalarp(double Sx,double Sy){
			//paso 1 trasladar la figura al origen
			int Tx=(int)fig2[1][0];
			int Ty=(int)fig2[1][1];
			Trasladar(-Tx,-Ty);
			//paso 2 apllicarle la transformacion
			escalarorigen(Sx,Sy);
			//paso 3 regresar la figura a la posicion original
			Trasladar(Tx,Ty);
		}
		//transfformaciones encadenadas
		//escalar xSx-TxSx+Tx,ySy-TySy+Ty
		public void EscalarE(double Sx,double Sy){
			double Tx=fig2[1][0];
			double Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*Sx-Tx*Sx+Tx;
				fig2[i][1]=y*Sy-Ty*Sy+Ty;
			}
		}
		//Rotar en contra
		//xcos-ysen-Txcos+tysen+tx,xsen+ycos-Txsen-Tycos+Ty
		public void rotarSentidoE(int ang)
		{
			double angR=Math.toRadians(ang);
			double coseno=Math.cos(angR);
			double seno=Math.sin(angR);
			double Tx=fig2[1][0],Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*coseno-y*seno-Tx*coseno+Ty*seno+Tx;
				fig2[i][1]=x*seno+y*coseno-Tx*seno-Ty*coseno+Ty;
			}
		}
		//rotar en sentido
		//xcos+ysen-Txcos-Tysen+Tx,-xsen+ycos+Txsen-Tycos+Ty
		public void rotarContraE(int ang)
		{
			double angR=Math.toRadians(ang);
			double coseno=Math.cos(angR);
			double seno=Math.sin(angR);
			double Tx=fig2[1][0],Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*coseno+y*seno-Tx*coseno-Ty*seno+Tx;
				fig2[i][1]=-x*seno+y*coseno-Tx*-seno-Ty*coseno+Ty;
			}
		}
		//reflexion
		//xRx-TyRx+Tx,yRy-TyRy+Ty
		public void ReflexionE(double Rx,double Ry){
			double Tx=fig2[1][0];
			double Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*Rx-Tx*Rx+Tx;
				fig2[i][1]=y*Ry-Ty*Ry+Ty;
			}
		}
		//Deformar
		//x+yDx-TyDx,xDy+y-TxDy
		public void DeformarE(double Dx,double Dy)
		{
			double Tx=fig2[1][0],Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x+y*Dx-Ty*Dx;
				fig2[i][0]=-x*Dy+y-Tx*Dy;
			}
		}
		public void mapeo(int xvmax,int xvmin,int xwmax,int yvmax,int yvmin,int ywmax,int op)
		{
			//xvmax=150;			xvmin=50;			yvmax=505;			yvmin=400;
			double Sx=(double)(xvmax-xvmin)/xwmax;
			double Sy=(double)(yvmax-yvmin)/ywmax;
			if(op==1)
				
			for(int i=0;i<fig2.length;i++)
			{
				figM[i][0]=fig2[i][0]*Sx+xvmin;
				figM[i][1]=fig2[i][1]*Sy+yvmin;
			}else
			for(int i=0;i<fig2.length;i++)
			{
				figM2[i][0]=fig2[i][0]*Sx+xvmin;
				figM2[i][1]=fig2[i][1]*Sy+yvmin;
			}
		}
}
