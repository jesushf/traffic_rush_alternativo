package Unidad2;
import java.applet.*;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.geom.*;
import java.awt.image.BufferedImage;
import java.net.*;
public class EjerColorTrazos extends Applet{
	Image img;
	Scrollbar barra;
	Color micolor;
	public void init()
	{
		setSize(800,700);
		setBackground(Color.LIGHT_GRAY);
		setLayout(new BorderLayout());
		barra=new Scrollbar(Scrollbar.VERTICAL,255,1,0,255);
		micolor=new Color(0,255,0,255);
		add(barra,BorderLayout.EAST);
		URL ruta=getClass().getResource("/Unidad2/recursos/estrella.png");
		img=getImage(ruta);
		barra.addAdjustmentListener(new AdjustmentListener(){
				public void adjustmentValueChanged(AdjustmentEvent e) {
					micolor=new Color(0,255,0,barra.getValue());
					repaint();
					
				}});
	}
	public void paint(Graphics g)
	{
		//GradientPaint(float x1,float y1,Color c1,float x2,float y2, Color c2);
		//GradientPaint(Point2D p1,Color c1,Point2D p2, Color c2);
		Graphics2D g2=(Graphics2D) g;
		Rectangle2D rect=new Rectangle2D.Double(100,100,200,150);
		GradientPaint horiz=new GradientPaint(200,101,Color.BLUE,200,150,Color.RED,true);
		g2.setPaint(horiz);
		g2.fill(rect);
		Rectangle2D rect2=new Rectangle2D.Double(400,300,200,150);
		GradientPaint vert=new GradientPaint(401,200,Color.GREEN,599,200,Color.YELLOW);
		g2.setPaint(vert);
		g2.fill(rect2);
		Rectangle2D rect3=new Rectangle2D.Double(650,100,200,150);
		GradientPaint diag=new GradientPaint(651,101,Color.MAGENTA,749,249,Color.ORANGE);
		g2.setPaint(diag);
		g2.fill(rect3);
		Rectangle2D transp=new Rectangle2D.Double(200,180,200,150);
		//TexturePaint(BufferImage image,Rectangle2D anchor);
		BufferedImage buffer=new BufferedImage(56,49,BufferedImage.TYPE_INT_ARGB);
		Graphics2D hoja=buffer.createGraphics();
		
		hoja.drawImage(img,0,0,this);
		TexturePaint textura=new TexturePaint(buffer,new Rectangle(56,49));
		Ellipse2D fig=new Ellipse2D.Double(100,300,250,200);
		g2.setPaint(textura);
		g2.fill(fig);
		//Transparencia
		g2.setPaint(micolor);
		g2.fill(transp);
		//Stroke-Trazos
		//BasicStroke(float width);
		g2.setPaint(Color.BLACK);
		float linea[]={15.0f,8.0f,10.0f};
		g2.setStroke(new BasicStroke(5,BasicStroke.CAP_BUTT,BasicStroke.JOIN_MITER,1,linea,0));
		GeneralPath rutaD=new GeneralPath();
		rutaD.moveTo(500, 500);
		rutaD.lineTo(500,600);
		rutaD.lineTo(600, 600);
		g2.draw(rutaD);
	}
}
