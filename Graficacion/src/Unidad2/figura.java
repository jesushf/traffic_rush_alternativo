package Unidad2;

import java.awt.Graphics;
import java.awt.Point;

class punto{
	int x,y;
	punto(int a,int b){
		x=a; y=b;
	}
}
public class figura {
	int fig1[]={200,180,250,220,200,260,280,260,280,220,260,180,200,180};
	double fig2[][]={{200,180},{250,220},{200,260},{280,260},{280,220},{260,180},{200,180}};
	punto fig3[]={new punto(200,180),new punto(250,220),new punto(200,260),new punto(280,260),
			new punto(280,220),new punto(260,180),new punto(200,180)};
	double original[][],figM[][];
	double minx,miny,maxx,maxy;
	public figura(){
		original=new double[fig2.length][fig2[0].length];
		figM=new double[fig2.length][fig2[0].length];
		for(int i=0;i<fig2.length;i++)
		{
			original[i][0]=fig2[i][0];
			original[i][1]=fig2[i][1];
		}
	}
	public void restaurar()
	{
		for(int i=0;i<fig2.length;i++){
			fig2[i][0]=original[i][0];
			fig2[i][1]=original[i][1];
		}
	}
	void dibujar1 (Graphics g){
		for(int x=0,y=1;x<fig1.length-3;x+=2,y+=2){
			g.drawLine(fig1[x], fig1[y], fig1[x+2],fig1[y+2] );
		}
			
	}
	void dibujar2 (Graphics g){
		for(int r=0;r<fig2.length-1;r++)
			g.drawLine((int)fig2[r][0], (int)fig2[r][1], (int)fig2[r+1][0],(int)fig2[r+1][1]);
	}
	void dibujarM (Graphics g){
		for(int r=0;r<figM.length-1;r++)
			g.drawLine((int)figM[r][0], (int)figM[r][1], (int)figM[r+1][0],(int)figM[r+1][1]);
	}
	void dibujar3 (Graphics g){
		for(int r=0;r<fig3.length-1;r++)
			g.drawLine(fig3[r].x, fig3[r].y, fig3[r+1].x,fig3[r+1].y);
	}
	//transformaciones
		//[x*Sx,y*Sy]
		public void escalarorigen(double Sx, double Sy)
		{
			for(int i=0;i<fig2.length;i++)
			{
				fig2[i][0]=fig2[i][0]*Sx;
				fig2[i][1]=fig2[i][1]*Sy;
			}
		}
		//x*cos+y*sen,-x*sen+y*cos
		public void rotarSMorigen(int ang){
			//ang->radianes
			double angR=Math.toRadians(ang);
			double senAng=Math.sin(angR);
			double cosAng=Math.cos(angR);
			for(int i=0;i<fig2.length;i++)
			{
				double X=fig2[i][0];
				double Y=fig2[i][1];
				fig2[i][0]=X*cosAng+Y*senAng;
				fig2[i][1]=-X*senAng+Y*cosAng;
			}
		}
		//x*cos-y*sen,x*sen+y*cos
		public void rotarCMorigen(int ang){
			double angR=Math.toRadians(ang);
			double senAng=Math.sin(angR);
			double cosAng=Math.cos(angR);
			for(int i=0;i<fig2.length;i++)
			{
				double X=fig2[i][0];
				double Y=fig2[i][1];
				fig2[i][0]=X*cosAng-Y*senAng;
				fig2[i][1]=X*senAng+Y*cosAng;
			}
		}
		//Deformacion
		//[x+y*Dy,x*Dx+y]
		public void Deformacion(double Dx, double Dy){
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x+y*Dy;
				fig2[i][1]=x*Dx+y;
			}
		}
		//Trasladar
		//[x+Tx,y+Ty]
		public void Trasladar(int Tx, int Ty){
			for(int i=0;i<fig2.length;i++){
				fig2[i][0]+=Tx;
				fig2[i][1]+=Ty;
			}
		}
		public boolean checar(Point t){
			//minx y miny
			minx=fig2[0][0];
			miny=fig2[0][1];
			for(int i=1;i<fig2.length;i++){
				if(fig2[i][0]<minx)
					minx=fig2[i][0];
				if(fig2[i][1]<miny)
					miny=fig2[i][1];
					}
			maxx=fig2[0][0];
			maxy=fig2[0][1];
			for(int i=1;i<fig2.length;i++){
				if(fig2[i][0]>maxx)
					maxx=fig2[i][0];
				if(fig2[i][1]>maxy)
					maxy=fig2[i][1];
					}
			if(t.x>minx && t.y>miny && t.x<maxx && t.y<maxy)
				return true;
			else
				return false;
		}
//Transformaciones con respecto a un punto
		public void rotarCMp(int ang){
			//paso 1 trasladar la figura al origen
			int Tx=(int)fig2[1][0];
			int Ty=(int)fig2[1][1];
			Trasladar(-Tx,-Ty);
			//paso 2 apllicarle la transformacion
			rotarCMorigen(ang);
			//paso 3 regresar la figura a la posicion original
			Trasladar(Tx,Ty);
		}
		public void Escalarp(double Sx,double Sy){
			//paso 1 trasladar la figura al origen
			int Tx=(int)fig2[1][0];
			int Ty=(int)fig2[1][1];
			Trasladar(-Tx,-Ty);
			//paso 2 apllicarle la transformacion
			escalarorigen(Sx,Sy);
			//paso 3 regresar la figura a la posicion original
			Trasladar(Tx,Ty);
		}
		//transfformaciones encadenadas
		//escalar xSx-TxSx+Tx,ySy-TySy+Ty
		public void EscalarE(double Sx,double Sy){
			double Tx=fig2[1][0];
			double Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*Sx-Tx*Sx+Tx;
				fig2[i][1]=y*Sy-Ty*Sy+Ty;
			}
		}
		//Rotar en contra
		//xcos-ysen-Txcos+tysen+tx,xsen+ycos-Txsen-Tycos+Ty
		public void rotarContraE(int ang)
		{
			double angR=Math.toRadians(ang);
			double coseno=Math.cos(angR);
			double seno=Math.sin(angR);
			double Tx=fig2[1][0],Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*coseno-y*seno-Tx*coseno+Ty*seno+Tx;
				fig2[i][0]=x*seno+y*coseno-Tx*seno-Ty*coseno+Ty;
			}
		}
		//rotar en sentido
		//xcos+ysen-Txcos-Tysen+Tx,-xsen+ycos+Txsen-Tycos+Ty
		public void rotarSentidoE(int ang)
		{
			double angR=Math.toRadians(ang);
			double coseno=Math.cos(angR);
			double seno=Math.sin(angR);
			double Tx=fig2[1][0],Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*coseno+y*seno-Tx*coseno-Ty*seno+Tx;
				fig2[i][0]=-x*seno+y*coseno+Tx*seno-Ty*coseno+Ty;
			}
		}
		//reflexion
		//xRx-TyRx+Tx,yRy-TyRy+Ty
		public void ReflexionE(double Rx,double Ry){
			double Tx=fig2[1][0];
			double Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x*Rx-Tx*Rx+Tx;
				fig2[i][1]=y*Ry-Ty*Ry+Ty;
			}
		}
		//Deformar
		//x+yDx-TyDx,xDy+y-TxDy
		public void DeformarE(double Dx,double Dy)
		{
			double Tx=fig2[1][0],Ty=fig2[1][1];
			for(int i=0;i<fig2.length;i++)
			{
				double x=fig2[i][0];
				double y=fig2[i][1];
				fig2[i][0]=x+y*Dx-Ty*Dx;
				fig2[i][0]=-x*Dy+y-Tx*Dy;
			}
		}
		public void mapeo(int xvmax,int xvmin,int xwmax,int yvmax,int yvmin,int ywmax)
		{
			double Sx=(double)(xvmax-xvmin)/xwmax;
			double Sy=(double)(yvmax-yvmin)/ywmax;
			for(int i=0;i<fig2.length;i++)
			{
				figM[i][0]=fig2[i][0]*Sx+xvmin;
				figM[i][1]=fig2[i][1]*Sy+yvmin;
			}
		}
}
