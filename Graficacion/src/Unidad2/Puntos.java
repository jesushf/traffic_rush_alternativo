package Unidad2;

import java.awt.*;
import java.awt.event.*;
import java.net.URL;
import java.util.ArrayList;

import javax.swing.*;

public class Puntos extends JPanel
{
	JFrame Vent;
	JButton b;
	int con=0, ind=-1, hoovered=-1;
	boolean mov=false;
	Point mPoint, cursor;
	ImageIcon img; //Imagen de tama�o original
	URL ruta=getClass().getResource("/Unidad2/estrella.png");
	
	ArrayList<Point> points = new ArrayList<Point>();
	double fig[][], fig2[][];
	
	public Puntos()
	{
		Vent=new JFrame("Ventana");
		this.setPreferredSize(new Dimension(720, 610));
		Vent.add(this);
		
		cursor=new Point(0, 0);
		b=new JButton("Print Coordinates");
		add(b);
		Iniciar();
		img=new ImageIcon(ruta);
		
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("\n");
				for(int i=0; i<points.size(); i++)
					if(i%10==9)
						System.out.println("{"+(int)points.get(i).getX()+","+(int)points.get(i).getY()+"},");
					else
						System.out.print("{"+(int)points.get(i).getX()+","+(int)points.get(i).getY()+"},");
				System.out.println();
			}
		});
		this.addMouseListener(new maneja(this));
		//this.addMouseMotionListener(new manejadora2(this));
		this.addMouseMotionListener(new MouseMotionListener() {
			public void mouseMoved(MouseEvent mme) {
				cursor=new Point(mme.getPoint());
				if(Comparar(mme.getPoint()))
					hoovered=ind;
				else
					hoovered=-1;
				repaint();
			}
			public void mouseDragged(MouseEvent e) {
				Point posa=e.getPoint();
				
				if(mov)
				{
					Point p=points.get(ind);
					int Tx=(int)(posa.x-mPoint.getX());
					int Ty=(int)(posa.y-mPoint.getY());
					points.set(ind, new Point((int)(p.getX()+Tx), (int)(p.getY()+Ty)));
					mPoint=new Point((int)(mPoint.getX()+Tx), (int)(mPoint.getY()+Ty));
					cursor=new Point(points.get(ind));
				}
				repaint();
			}
		});
		Vent.pack();
		Vent.setLocationRelativeTo(null);
		Vent.setVisible(true);
		Vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		img.paintIcon(this, g, 0, 0);
		
		g.setColor(Color.BLACK);
		g.drawString("#Puntos: "+points.size(), 500, 20);
		g.drawString("Cursor: "+(int)cursor.getX()+","+(int)cursor.getY(), 500, 60);
		if(hoovered!=-1)
			g.drawString("En punto #: "+hoovered, 500, 40);
		
		g.setColor(Color.GREEN);
		for(int i=1; i<points.size()-1; i++)
		{
			g.drawOval((int)(points.get(i).getX()-3), (int)(points.get(i).getY()-3), 6, 6);
		}
		if(points.size()>0)
		{
			g.setColor(Color.PINK);
			g.drawOval((int)(points.get(0).getX()-3), (int)(points.get(0).getY()-3), 6, 6);
			g.drawOval((int)(points.get(points.size()-1).getX()-3), (int)(points.get(points.size()-1).getY()-3), 6, 6);
		}
		
		g.setColor(Color.BLACK);
		for(int i=1; i<points.size(); i++)
			g.drawLine((int)points.get(i-1).getX(), (int)points.get(i-1).getY(), (int)points.get(i).getX(), (int)points.get(i).getY());
		for(int i=1; i<fig2.length; i++)
			g.drawLine((int)fig2[i-1][0], (int)fig2[i-1][1], (int)fig2[i][0], (int)fig2[i][1]);
		
	}
	public boolean Comparar(Point click)
	{
		for(int i=0; i<points.size(); i++)
			if(click.getX()>=points.get(i).getX()-3 && click.getX()<=points.get(i).getX()+3 &&
					click.getY()>=points.get(i).getY()-3 && click.getY()<=points.get(i).getY()+3)
			{
				ind=i;
				return true;
			}
		return false;
	}
	public void Iniciar()
	{
		double tmp[][]={};
		fig=tmp;
		double tmp2[][]={{110,400},{95,415},{105,410},{90,425},{100,420},
				{85,435},{95,430},{80,445},{90,440},{75,455},{85,450},
				{70,465},{80,460},{65,475},{75,470},{60,485},{70,480},
				{55,495},{65,490},{50,505},{60,500},{90,500},{110,485},
				{100,490},{115,475},{105,480},{120,465},{110,470},
				{125,455},{115,460},{130,445},{120,450},{135,435},{125,440},
				{140,425},{130,430},{145,415},{135,420},{150,405},{110,400}};
		fig2=tmp2;
		//{245,154},{235,182},{208,192},{236,202},
		double tmp3[][]={};
		//fig=tmp3;
		for(int i=0; i<fig.length; i++)
		{
			points.add(new Point((int)fig[i][0], (int)fig[i][1]));
		}
		
	}
	public static void main(String[] args)
	{
		new Puntos();
	}
}

class maneja extends MouseAdapter
{
	Puntos obj;
	public maneja(Puntos T)
	{
		obj=T;
	}
	public void mousePressed(MouseEvent e)
	{
		Point pres=e.getPoint();
		obj.mov=obj.Comparar(pres);
		
		if(obj.mov)
			obj.mPoint=pres;
		else
		{
			System.out.print("{"+(int)pres.getX()+","+(int)pres.getY()+"},");
			obj.con++;
			if(obj.con>5)
			{
				obj.con=0;
				System.out.println();
			}
			obj.points.add(pres);
		}
		obj.repaint();
	}
	public void mouseClicked(MouseEvent e)
	{
		if(SwingUtilities.isRightMouseButton(e))
		{
			obj.points.remove(obj.ind);
		}
	}
}
