package Unidad2;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.net.URL;
import javax.swing.*;

class help extends JFrame{
	String arr[]={"Rotar 5 Grados a la derecha la figura: dar dos clicks a la derecha de la figura, pulsar la flecha de rotacion",
			"Rotar 5 Grados a la izquierda :dar dos clicks a la izquierda de la figura, pulsar la flecha de rotacion",
			"Aumenta la figura .1 : pulsar la lupa de zoom+","Disminuye la figura .1 : pulsar la lupa de zoom-",
			"Refleja la figura con los valores -1,-1 :pulsar la imagen del gato", "Escalar la figura: atajo: CTRL+E",
			"Deforma la figura .1 en el eje x y .1 en en el eje y : pulsar la imagen de deformacion",
			"Traslada la figura 10 en X y 10 en Y :pulsar la imagen de translacion de la tierra","Ventana de ayuda atajo: ALT+F1",
			"Restaurar la figura atajo: CTRL+R","Rotar la figura un angulo especifico atajo: CTRL+A","Deformar la figura atajo: CTRL+F",
			"Reflejar la figura atajo: CTRL+D","Trasladar la figura atajo: CTRL+G","Cambiar el fondo: pulsar las flechas de cambio en rojo y verde"};
	public help()
	{
		JButton aceptar=new JButton("aceptar");
		setSize(800,600);
		JPanel p=new JPanel();
		p.setLayout(new GridLayout(arr.length,1));
		JLabel l[]=new JLabel[arr.length];
		for(int i=0;i<l.length;i++)
		{
			l[i]=new JLabel(arr[i]);
			p.add(l[i]);
		}
		add(p,BorderLayout.CENTER);
		JPanel b=new JPanel();
		b.setLayout(new FlowLayout());
		b.add(aceptar);
		add(b,BorderLayout.SOUTH);
		aceptar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent d)
			{
				invisible();
			}
		});
		//setVisible(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	public void visible()
	{
		setVisible(true);
	}
	public void invisible()
	{
		setVisible(false);
	}
}
public class Unidad2proyaffine extends JPanel implements MouseListener, MouseWheelListener, MouseMotionListener,ActionListener{
	int figura[][]={{210,200},{195,215},{205,210},{190,225},{200,220},
			{185,235},{195,230},{180,245},{190,240},{175,255},{185,250},
			{170,265},{180,260},{165,275},{175,270},{160,285},{170,280},
			{155,295},{165,290},{150,305},{160,300},{190,300},{210,285},
			{200,290},{215,275},{205,280},{220,265},{210,270},
			{225,255},{215,260},{230,245},{220,250},{235,235},{225,240},
			{240,225},{230,230},{245,215},{235,220},{250,205},{210,200}};
	Point2D pc1,pc2;
	GeneralPath gp;
	Shape F;
	int original[][];
	help oby=new help();
	boolean mover=true,rotar=true,def=true,cambio=true;
	Image imagen;
	JButton brefl,bdefor,btrasl,brd,bri,be1,be2,bcambio;
	JToolBar barrah;
	JMenuBar barraM;
	JFrame vent;
	public Unidad2proyaffine()
	{
		vent=new JFrame("AffineTransform proyecto");
		vent.setSize(1000,720);
		setSize(1000,720);
		URL ruta=getClass().getResource("/Unidad2/recursos/lienzo2.jpg/");
		Toolkit tk=Toolkit.getDefaultToolkit();
		imagen=tk.getImage(ruta);
		//repaint();
		barraherr();
		barraM=new JMenuBar ();
		ponerMenu();
		vent.setJMenuBar(barraM);
		pc1=new Point2D.Double(500,250);
		pc2=new Point2D.Double(300,250);
		original=new int[figura.length][2];
		for(int i=0;i<figura.length;i++)
		{
			original[i][0]=figura[i][0];
			original[i][1]=figura[i][1];
		}
		gp=new GeneralPath();
		gp.moveTo(figura[0][0], figura[0][1]);
		for(int i=1;i<=figura.length-2;i++)
			gp.lineTo(figura[i][0], figura[i][1]);
		gp.lineTo(figura[figura.length-1][0], figura[figura.length-1][1]);
		F=gp;
		setVisible(true);
		vent.add(this);
		vent.setVisible(true);
		vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addMouseListener(this);
		addMouseWheelListener(this);
		addMouseMotionListener(this);
	}
	public void ponerMenu()
	{
		JMenu Transformar,Opciones;
		Transformar =new JMenu("Transformar");
		Opciones=new JMenu("Opciones");
		barraM.add(Transformar);
		//barraM.add(Opciones);
		JMenuItem op1,op2,op3,op4,op5,op6,op7;
		URL ruta=getClass().getResource("/Unidad2/recursos/restaurar.jpg");
		Action ac1=new AbstractAction("Restaurar",new ImageIcon(ruta)){
			public void actionPerformed(ActionEvent e)
			{
				restaurar();
				repaint();
			}
	};
	ac1.putValue(Action.MNEMONIC_KEY, new Integer('R'));
	ac1.putValue(Action.SHORT_DESCRIPTION, "Restaura la figura");
	ac1.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R,InputEvent.CTRL_MASK));
		op1=new JMenuItem("Restaurar");
		op1.setAction(ac1);
		Transformar.add(op1);
		URL ruta2=getClass().getResource("/Unidad2/recursos/zoomm.jpg");
		Action ac2=new AbstractAction("Escalar",new ImageIcon(ruta2)){
			public void actionPerformed(ActionEvent e)
			{
				String fs=JOptionPane.showInputDialog("Dame la cantidad a Escalar ");
				double Rx=1;
				try{
			    	Rx=Double.parseDouble(fs);
			    }catch(NumberFormatException x){
			    	
			    }
				escalar(Rx);
				repaint();
			}
	};
	
	ac2.putValue(Action.MNEMONIC_KEY, new Integer('E'));
	ac2.putValue(Action.SHORT_DESCRIPTION, "Escalar la figura");
	ac2.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
		op2=new JMenuItem("Escalar");
		op2.setAction(ac2);
		Transformar.add(op2);
		
		URL ruta3=getClass().getResource("/Unidad2/recursos/rotard.jpg");
		Action ac3=new AbstractAction("Rotar",new ImageIcon(ruta3)){
			public void actionPerformed(ActionEvent e)
			{
				cuadroDialogo obj=new cuadroDialogo(vent,true);
				if(obj.valores[0]==0)
				{
					rotar(obj.valores[1]);
				}
					
				else
				{
					rotar(-obj.valores[1]);
				}
				repaint();
			}
	};
	
	ac3.putValue(Action.MNEMONIC_KEY, new Integer('A'));
	ac3.putValue(Action.SHORT_DESCRIPTION, "Rotar la figura");
	ac3.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A,InputEvent.CTRL_MASK));
		op3=new JMenuItem("Rotar");
		op3.setAction(ac3);
		Transformar.add(op3);
	
		URL ruta4=getClass().getResource("/Unidad2/recursos/ayuda.png");
		Action ac4=new AbstractAction("Ayuda",new ImageIcon(ruta4)){
			public void actionPerformed(ActionEvent e)
			{
				oby.visible();
				repaint();
			}
	};
	
	//ac4.putValue(Action.MNEMONIC_KEY, new Integer('A'));
	ac4.putValue(Action.SHORT_DESCRIPTION, "Ventana de ayuda");
	ac4.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F1,InputEvent.ALT_MASK));
		op4=new JMenuItem("Ayuda");
		op4.setAction(ac4);
		Transformar.add(op4);
		
		URL ruta5=getClass().getResource("/Unidad2/recursos/reflejar.jpg");
		Action ac5=new AbstractAction("Reflejar",new ImageIcon(ruta5)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a trasladar en Y");
					    int Rx=0,Ry=0;
					    try{
					    	Rx=Integer.parseInt(fsx);
					    	Ry=Integer.parseInt(fsy);
					    }catch(NumberFormatException x){
					    	Rx=Ry=0;
					    }
					    //System.out.println(Rx+Ry);
				reflexion(Rx,Ry);
				repaint();
			}
	};
	
	ac5.putValue(Action.MNEMONIC_KEY, new Integer('D'));
	ac5.putValue(Action.SHORT_DESCRIPTION, "Reflejar la figura");
	ac5.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D,InputEvent.CTRL_MASK));
		op5=new JMenuItem("Reflejar");
		op5.setAction(ac5);
		Transformar.add(op5);
		
		URL ruta6=getClass().getResource("/Unidad2/recursos/deformar.png");
		Action ac6=new AbstractAction("Deformar",new ImageIcon(ruta6)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a deformar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a deformar en Y");
					    double dx=0,dy=0;
					    try{
					    	dx=Double.parseDouble(fsx);
					    	dy=Double.parseDouble(fsy);
					    }catch(NumberFormatException x){
					    	dx=dy=0;
					    }
					   deformar(dx,dy);
					    repaint();
			}
	};
	
	ac6.putValue(Action.MNEMONIC_KEY, new Integer('F'));
	ac6.putValue(Action.SHORT_DESCRIPTION, "Deformar la figura");
	ac6.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F,InputEvent.CTRL_MASK));
		op6=new JMenuItem("Deformar");
		op6.setAction(ac6);
		Transformar.add(op6);
		
		URL ruta7=getClass().getResource("/Unidad2/recursos/trasladar.jpg");
		Action ac7=new AbstractAction("Trasladar",new ImageIcon(ruta7)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en Y");
					    int Tx=0,Ty=0;
					    try{
					    	Tx=Integer.parseInt(fsx);
					    	Ty=Integer.parseInt(fsy);
					    }catch(NumberFormatException x){
					    	Tx=Ty=0;
					    }
					    Point2D punto=new Point2D.Double(Tx+F.getBounds().getCenterX(),Ty+F.getBounds().getCenterY());
					   trasladar(punto);
					    repaint();
			}
	};
	
	ac7.putValue(Action.MNEMONIC_KEY, new Integer('G'));
	ac7.putValue(Action.SHORT_DESCRIPTION, "Trasladar la figura");
	ac7.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G,InputEvent.CTRL_MASK));
		op7=new JMenuItem("Trasladar");
		op7.setAction(ac7);
		Transformar.add(op7);
	}
	public void restaurar()
	{
		for(int i=0;i<figura.length;i++)
		{
			figura[i][0]=original[i][0];
			figura[i][1]=original[i][1];
		}
		gp=new GeneralPath();
		gp.moveTo(figura[0][0], figura[0][1]);
		for(int i=1;i<=figura.length-2;i++)
			gp.lineTo(figura[i][0], figura[i][1]);
		gp.lineTo(figura[figura.length-1][0], figura[figura.length-1][1]);
		F=gp;
	}
	public void deformar(double A,double B)
	{
		AffineTransform at=new AffineTransform();
		at.shear(A,B);
		F=at.createTransformedShape(F);
	}
	public void barraherr()
	{
		URL rutaImg=getClass().getResource("/Unidad2/recursos/rotari.jpg");
		   brd=new JButton();
		   brd.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/rotard.jpg");
		   bri=new JButton();
		   bri.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/zoomm.jpg");
		   be1=new JButton();
		   be1.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/zoomme.jpg");
		   be2=new JButton();
		   be2.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/reflejar.jpg");
		   brefl=new JButton();
		   brefl.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/deformar.png");
		   bdefor=new JButton();
		   bdefor.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/trasladar.jpg");
		   btrasl=new JButton();
		   btrasl.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/ewdsssssasssszzxvcxvd.jpg");
		   bcambio=new JButton();
		   bcambio.setIcon(new ImageIcon(rutaImg));
		   barrah=new JToolBar("Operacines Rapidas",JToolBar.VERTICAL);
		   barrah.setFloatable(false);
		   
		   
		   barrah.add(brd);barrah.add(bri);barrah.add(be1);barrah.add(be2);
		   barrah.add(brefl);barrah.add(bdefor);barrah.add(btrasl);barrah.add(bcambio);
		   vent.add(barrah,BorderLayout.EAST);
		   brd.setToolTipText("Rotar 5 Grados a la derecha");
		   bri.setToolTipText("Rotar 5 Grados a la izquierda");
		   be1.setToolTipText("Escala la figura .1");
		   be2.setToolTipText("Disminuye la figura .1");
		   brefl.setToolTipText("Refleja la figura con los valores -1,-1");
		   bdefor.setToolTipText("Deforma la figura 0.5 en el eje x y 0.7 en en el eje y");
		   btrasl.setToolTipText("Traslada la figura 10 en X y 10 en Y");
		   bcambio.setToolTipText("cambia el fondo");
		   bcambio.addActionListener(this);
		   brd.addActionListener(this);
		   bri.addActionListener(this);
		   be1.addActionListener(this);
		   be2.addActionListener(this);
		   brefl.addActionListener(this);
		   bdefor.addActionListener(this);
		   btrasl.addActionListener(this);
	}
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(imagen, 0, 0, this.getSize().width, this.getSize().height,this);
				Graphics2D g2= (Graphics2D)g;
		/*gp.moveTo(figura[0][0], figura[0][1]);
		for(int i=1;i<=6;i++)
			gp.lineTo(figura[i][0], figura[i][1]);
		gp.curveTo(figura[6][0], figura[6][1], pc1.getX(), pc1.getY(), figura[7][0], figura[7][1]);
		gp.lineTo(figura[8][0], figura[8][1]);
		gp.curveTo(figura[8][0], figura[8][1], pc2.getX(), pc2.getY(), figura[0][0], figura[0][1]);
		F=gp;*/
		g2.setPaint(Color.YELLOW);
		g2.fill(F);
		g2.setPaint(Color.BLUE);
		g2.setStroke(new BasicStroke(3));
		g2.draw(F);
	
	}
	public void mouseClicked(MouseEvent e) {
		rotar=!rotar;
		if(rotar){
		int X=e.getX();
		if(X>F.getBounds2D().getMaxX())
			rotar(5);
		else
			if(X<F.getBounds2D().getMinX())
				rotar(-5);}
		/*int Y=e.getY();
		if(Y<F.getBounds2D().getMinY())
			reflexion(1,-1);
		else
			if(Y>F.getBounds2D().getMinY())
				reflexion(1,-1);*/
		repaint();
	}
	public void reflexion(double ref1,double ref2)
	{
		AffineTransform at=new AffineTransform();
		at.translate(F.getBounds2D().getCenterX(),F.getBounds2D().getCenterY());
		at.scale(ref1, ref2);
		at.translate(-F.getBounds2D().getCenterX(), -F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	public void rotar(int grados)
	{
		double angR=Math.toRadians((double)grados);
		AffineTransform at=new AffineTransform();
		at.setToRotation(angR,F.getBounds2D().getCenterX(),F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	public void mousePressed(MouseEvent e) {
		Point2D pclick=e.getPoint();
		if(F.contains(pclick))
			mover=true;
		else mover=false;
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int notches=e.getWheelRotation();
		if(notches<0)
			escalar(.9);
		else
			escalar(1.1);
		repaint();
	}
	public void escalar(double S)
	{
		AffineTransform at=new AffineTransform();
		//REGRESAR LA FIGURA A LA POSICION ORIGINAL
		at.translate(F.getBounds2D().getCenterX(), F.getBounds2D().getCenterY());
		//APLICAR LA ESCALA
		at.scale(S,S);
		//mover al origen la figura la original
		at.translate(-F.getBounds2D().getCenterX(), -F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		Point2D pmover=e.getPoint();
		if(mover)
			trasladar(pmover);
		repaint();
	}
	public void trasladar(Point2D pm)
	{
		AffineTransform at=new AffineTransform();
		at.setToTranslation(pm.getX()-F.getBounds2D().getCenterX(),pm.getY()-F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		
	}
	public static void main(String x[])
	{
		new Unidad2proyaffine();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==brd)
		{
			rotar(5);
		}
		else
			if(e.getSource()==bri)
			{
				rotar(-5);
			}
			else
				if(e.getSource()==be1)
				{
					escalar(1.1);
				}
				else
					if(e.getSource()==be2)
					{
						escalar(0.9);
					}
		if(e.getSource()==brefl)
		{
			reflexion(-1,-1);
		}else
			if(e.getSource()==btrasl)
			{
				Point2D punto=new Point2D.Double(F.getBounds().getCenterX()+10,F.getBounds().getCenterY()+10);
				trasladar(punto);
			}
			else
			if(e.getSource()==bdefor)
			{
				deformar(0.1,0.1);
			}
			else
		if(e.getSource()==bcambio)
		{
			cambio=!cambio;
			if(!cambio)
			{
				URL ruta=getClass().getResource("/Unidad2/recursos/lienzo1.jpg/");
				Toolkit tk=Toolkit.getDefaultToolkit();
				imagen=tk.getImage(ruta);
			}
			else{
				URL ruta=getClass().getResource("/Unidad2/recursos/lienzo2.jpg/");
			Toolkit tk=Toolkit.getDefaultToolkit();
			imagen=tk.getImage(ruta);
			}
			
		}
		
		repaint();
	}
}
