package Unidad2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.*;
import java.net.URL;
import javax.swing.*;


/*class tec extends KeyAdapter{
	Transformar obj;
	public tec(Transformar T){
		obj=T;
	}
	public void keyPressed(KeyEvent e){
		int code=e.getKeyCode();
		if(code==KeyEvent.VK_F1)
		{
			obj.oby.visible();
		}
	}
}*/
class ayuda extends JFrame{
	String arr[]={"Rotar 5 Grados a la derecha la figura: dar dos clicks, pulsar la flecha de rotacion",
			"Rotar 5 Grados a la izquierda :pulsar la flecha de rotacion",
			"Escala la figura .1 : pulsar la lupa de zoom+ atajo: CTRL+E","Disminuye la figura .1 : pulsar la lupa de zoom-",
			"Refleja la figura con los valores -1,-1 :pulsar la imagen del gato",
			"Deforma la figura 0.5 en el eje x y 0.7 en en el eje y : pulsar la imagen de deformacion",
			"Traslada la figura 10 en X y 10 en Y :pulsar la imagen de translacion de la tierra","Ventana de ayuda atajo: ALT+F1",
			"Restaurar la figura atajo: CTRL+R","Rotar la figura un angulo especifico atajo: CTRL+A","Deformar la figura atajo: CTRL+F",
			"Reflejar la figura atajo: CTRL+D","Trasladar la figura atajo: CTRL+G",};
	public ayuda()
	{
		JButton aceptar=new JButton("aceptar");
		setSize(600,400);
		JPanel p=new JPanel();
		p.setLayout(new GridLayout(arr.length,1));
		JLabel l[]=new JLabel[arr.length];
		for(int i=0;i<l.length;i++)
		{
			l[i]=new JLabel(arr[i]);
			p.add(l[i]);
		}
		add(p,BorderLayout.CENTER);
		JPanel b=new JPanel();
		b.setLayout(new FlowLayout());
		b.add(aceptar);
		add(b,BorderLayout.SOUTH);
		aceptar.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent d)
			{
				invisible();
			}
		});
		//setVisible(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	public void visible()
	{
		setVisible(true);
	}
	public void invisible()
	{
		setVisible(false);
	}
}
class manejar extends MouseAdapter{
	Transformar obj;
	public manejar(Transformar T) {
		obj=T;
	}
	public void mousePressed(MouseEvent e) {
		Point pres=e.getPoint();
	    if(obj.fig.checar(pres))
		    obj.mover=true;
		else
			obj.mover=false;
	    //System.out.println("dentro: "+obj.mover);
	   // if(!obj.mover)
	    obj.rotar=!(obj.rotar);
	    if(obj.rotar)
	    {
	    	obj.fig.rotarSentidoE(5);
	    	obj.repaint();
	    }
	}
}
class manejar2 extends MouseMotionAdapter{
	Transformar obj;
	public manejar2(Transformar T) {
		obj=T;
	}
	public void mouseDragged(MouseEvent e) {
		Point posa=e.getPoint();
		if(obj.mover){
			obj.fig.Trasladar((int)(posa.x-obj.fig.fig2[1][0]),
					(int)(posa.y-obj.fig.fig2[1][1]));
		}
		obj.repaint(); 
	}
}
public class Transformar extends JPanel implements ActionListener, MouseWheelListener{
	JFrame Vent;
	//tec obt=new tec(this);
	Image imagen;
	proyunidad2figura fig;
	JButton besc,brotc,brots,bdef,btras,brest,bref,cambio,brefl,bdefor,btrasl;
	boolean mover=false,rotar=true;
	JToolBar barrah;
	JMenuBar barraM;
	JButton brd,bri,be1,be2;
	int opr=0,	ang=5, op=1;
	ayuda oby=new ayuda();
	public void barraherr()
	{
		URL rutaImg=getClass().getResource("/Unidad2/recursos/rotai.png");
		   brd=new JButton();
		   brd.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/rotad.png");
		   bri=new JButton();
		   bri.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/zoomm.jpg");
		   be1=new JButton();
		   be1.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/zoomme.jpg");
		   be2=new JButton();
		   be2.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/reflejar.jpg");
		   brefl=new JButton();
		   brefl.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/deformar.png");
		   bdefor=new JButton();
		   bdefor.setIcon(new ImageIcon(rutaImg));
		   rutaImg=getClass().getResource("/Unidad2/recursos/trasladar.jpg");
		   btrasl=new JButton();
		   btrasl.setIcon(new ImageIcon(rutaImg));
		   barrah=new JToolBar("Operacines Rapidas",JToolBar.VERTICAL);
		   barrah.setFloatable(false);
		   Vent.add(barrah,BorderLayout.EAST);
		   
		   barrah.add(brd);barrah.add(bri);barrah.add(be1);barrah.add(be2);
		   barrah.add(brefl);barrah.add(bdefor);barrah.add(btrasl);
		   brd.setToolTipText("Rotar 5 Grados a la derecha");
		   bri.setToolTipText("Rotar 5 Grados a la izquierda");
		   be1.setToolTipText("Escala la figura .1");
		   be2.setToolTipText("Disminuye la figura .1");
		   brefl.setToolTipText("Refleja la figura con los valores -1,-1");
		   bdefor.setToolTipText("Deforma la figura 0.5 en el eje x y 0.7 en en el eje y");
		   btrasl.setToolTipText("Traslada la figura 10 en X y 10 en Y");
		   brd.addActionListener(this);
		   bri.addActionListener(this);
		   be1.addActionListener(this);
		   be2.addActionListener(this);
		   brefl.addActionListener(this);
		   bdefor.addActionListener(this);
		   btrasl.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==brd)
		{
			fig.rotarContraE(5);
		}
		else
			if(e.getSource()==bri)
			{
				fig.rotarSentidoE(5);
			}
			else
				if(e.getSource()==be1)
				{
					fig.EscalarE(1.1, 1.1);
				}
				else
					if(e.getSource()==be2)
					{
						fig.EscalarE(0.9, 0.9);
					}
					else
						if(e.getSource()==bdefor)
						{
							fig.DeformarE(0.5, 0.7);
						}
						else
							if(e.getSource()==brefl)
							{
								fig.ReflexionE(-1,-1);
							}
							else
								if(e.getSource()==btrasl)
								{
									 fig.Trasladar(10, 10);
								}
		repaint();
	}
	public void ponerMenu()
	{
		JMenu Transformar,Opciones;
		Transformar =new JMenu("Transformar");
		Opciones=new JMenu("Opciones");
		barraM.add(Transformar);
		//barraM.add(Opciones);
		JMenuItem op1,op2,op3,op4,op5,op6,op7;
		URL ruta=getClass().getResource("/Unidad2/recursos/restaurar.jpg");
		Action ac1=new AbstractAction("Restaurar",new ImageIcon(ruta)){
			public void actionPerformed(ActionEvent e)
			{
				fig.restaurar(op);
				repaint();
			}
	};
	ac1.putValue(Action.MNEMONIC_KEY, new Integer('R'));
	ac1.putValue(Action.SHORT_DESCRIPTION, "Restaura la figura");
	ac1.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_R,InputEvent.CTRL_MASK));
		op1=new JMenuItem("Restaurar");
		op1.setAction(ac1);
		Transformar.add(op1);
		URL ruta2=getClass().getResource("/Unidad2/recursos/zoomm.jpg");
		Action ac2=new AbstractAction("Escalar",new ImageIcon(ruta2)){
			public void actionPerformed(ActionEvent e)
			{
				fig.EscalarE(1.1,1.1);
				repaint();
			}
	};
	
	ac2.putValue(Action.MNEMONIC_KEY, new Integer('E'));
	ac2.putValue(Action.SHORT_DESCRIPTION, "Escalar la figura");
	ac2.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_E,InputEvent.CTRL_MASK));
		op2=new JMenuItem("Escalar");
		op2.setAction(ac2);
		Transformar.add(op2);
		
		URL ruta3=getClass().getResource("/Unidad2/recursos/rotad.png");
		Action ac3=new AbstractAction("Rotar",new ImageIcon(ruta3)){
			public void actionPerformed(ActionEvent e)
			{
				cuadroDialogo obj=new cuadroDialogo(Vent,true);
				if(obj.valores[0]==0)
				{
					fig.rotarSentidoE(obj.valores[1]);
				}
					
				else
				{
					fig.rotarContraE(obj.valores[1]);
				}
				repaint();
			}
	};
	
	ac3.putValue(Action.MNEMONIC_KEY, new Integer('A'));
	ac3.putValue(Action.SHORT_DESCRIPTION, "Rotar la figura");
	ac3.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_A,InputEvent.CTRL_MASK));
		op3=new JMenuItem("Rotar");
		op3.setAction(ac3);
		Transformar.add(op3);
	
		URL ruta4=getClass().getResource("/Unidad2/recursos/ayuda.png");
		Action ac4=new AbstractAction("Ayuda",new ImageIcon(ruta4)){
			public void actionPerformed(ActionEvent e)
			{
				oby.visible();
				repaint();
			}
	};
	
	//ac4.putValue(Action.MNEMONIC_KEY, new Integer('A'));
	ac4.putValue(Action.SHORT_DESCRIPTION, "Ventana de ayuda");
	ac4.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F1,InputEvent.ALT_MASK));
		op4=new JMenuItem("Ayuda");
		op4.setAction(ac4);
		Transformar.add(op4);
		
		URL ruta5=getClass().getResource("/Unidad2/recursos/reflejar.jpg");
		Action ac5=new AbstractAction("Reflejar",new ImageIcon(ruta5)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a trasladar en Y");
					    int Rx=0,Ry=0;
					    try{
					    	Rx=Integer.parseInt(fsx);
					    	Ry=Integer.parseInt(fsy);
					    }catch(NumberFormatException x){
					    	Rx=Ry=0;
					    }
					    //System.out.println(Rx+Ry);
				fig.ReflexionE(Rx, Ry);
				repaint();
			}
	};
	
	ac5.putValue(Action.MNEMONIC_KEY, new Integer('D'));
	ac5.putValue(Action.SHORT_DESCRIPTION, "Reflejar la figura");
	ac5.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_D,InputEvent.CTRL_MASK));
		op5=new JMenuItem("Reflejar");
		op5.setAction(ac5);
		Transformar.add(op5);
		
		URL ruta6=getClass().getResource("/Unidad2/recursos/deformar.png");
		Action ac6=new AbstractAction("Deformar",new ImageIcon(ruta6)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a deformar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a deformar en Y");
					    double dx=0.0,dy=0.0;
					    try{
					    	dx=Double.parseDouble(fsx);
					    	dy=Double.parseDouble(fsy);
					    }catch(NumberFormatException x){
					    	dx=dy=0.0;
					    }
					    fig.DeformarE(dx, dy);
					    repaint();
			}
	};
	
	ac6.putValue(Action.MNEMONIC_KEY, new Integer('F'));
	ac6.putValue(Action.SHORT_DESCRIPTION, "Deformar la figura");
	ac6.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F,InputEvent.CTRL_MASK));
		op6=new JMenuItem("Deformar");
		op6.setAction(ac6);
		Transformar.add(op6);
		
		URL ruta7=getClass().getResource("/Unidad2/recursos/trasladar.jpg");
		Action ac7=new AbstractAction("Trasladar",new ImageIcon(ruta7)){
			public void actionPerformed(ActionEvent e)
			{
				String fsx=JOptionPane.showInputDialog(
						  "Dame la cantidad a Trasladar en X");
						String fsy=JOptionPane.showInputDialog(
						  "Dame la cantidad a trasladar en Y");
					    int Tx=0,Ty=0;
					    try{
					    	Tx=Integer.parseInt(fsx);
					    	Ty=Integer.parseInt(fsy);
					    }catch(NumberFormatException x){
					    	Tx=Ty=0;
					    }
					    fig.Trasladar(Tx, Ty);
					    repaint();
			}
	};
	
	ac7.putValue(Action.MNEMONIC_KEY, new Integer('G'));
	ac7.putValue(Action.SHORT_DESCRIPTION, "Trasladar la figura");
	ac7.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G,InputEvent.CTRL_MASK));
		op7=new JMenuItem("Trasladar");
		op7.setAction(ac7);
		Transformar.add(op7);
	}
	public Transformar(){
	   Vent=new JFrame("Transformar en 2D");	
	   Vent.setSize(1000, 600);
	   barraherr();
	   fig=new proyunidad2figura();
	   Vent.add(this);
	   barraM=new JMenuBar ();
	   Vent.setJMenuBar(barraM);
	   ponerMenu();
	   besc=new JButton("Escalar");
	   brotc=new JButton("Rotar en Contra");
	   brots=new JButton("Rotar en el Sentido");
	   bdef=new JButton("Deformar");
	   btras=new JButton("Trasladar");
	   bref=new JButton("Reflexion");
	   brest=new JButton("Restaurar");
	   cambio=new JButton("cambiar figura");
	   JPanel gb=new JPanel(); 
	   gb.setLayout(new FlowLayout());
	   gb.add(brest);
	   gb.add(besc);gb.add(brotc);gb.add(brots);
	   gb.add(bdef);gb.add(bref);gb.add(btras);gb.add(cambio);
	   Vent.add(gb,BorderLayout.SOUTH);
	   URL ruta=getClass().getResource("/Unidad2/recursos/pista.jpg/");
		
		Toolkit tk=Toolkit.getDefaultToolkit();
		imagen=tk.getImage(ruta);
	   brest.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			fig.restaurar(op);
			repaint();
		}});
	   brotc.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
			String cang=JOptionPane.showInputDialog(
					"Dame en angulo de rotaci�n");
			int ang=0;
			try{
				ang=Integer.parseInt(cang);
			}catch(NumberFormatException ex){
			    ang=0;	
			}
			fig.rotarContraE(ang);
			repaint();
		}});
	   brots.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				String cang=JOptionPane.showInputDialog(
				"Dame en angulo de rotaci�n");
				int ang=0;
				try{
					ang=Integer.parseInt(cang);
				}catch(NumberFormatException ex){
					ang=0;	
				}
				fig.rotarSentidoE(ang);
				repaint();
			}});
	   besc.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e) {
				String fsx=JOptionPane.showInputDialog(
				  "Dame la cantidad a escalar en X");
				String fsy=JOptionPane.showInputDialog(
				  "Dame la cantidad a escalar en Y");
			    double sx=0.0,sy=0.0;
			    try{
			    	sx=Double.parseDouble(fsx);
			    	sy=Double.parseDouble(fsy);
			    }catch(NumberFormatException x){
			    	sx=sy=1.0;
			    }
			    fig.EscalarE(sx, sy);
			    repaint();
		}});
	   bdef.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					String fsx=JOptionPane.showInputDialog(
					  "Dame la cantidad a deformar en X");
					String fsy=JOptionPane.showInputDialog(
					  "Dame la cantidad a deformar en Y");
				    double dx=0.0,dy=0.0;
				    try{
				    	dx=Double.parseDouble(fsx);
				    	dy=Double.parseDouble(fsy);
				    }catch(NumberFormatException x){
				    	dx=dy=0.0;
				    }
				    fig.DeformarE(dx, dy);
				    repaint();
			}});
	   btras.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					String fsx=JOptionPane.showInputDialog(
					  "Dame la cantidad a Trasladar en X");
					String fsy=JOptionPane.showInputDialog(
					  "Dame la cantidad a trasladar en Y");
				    int Tx=0,Ty=0;
				    try{
				    	Tx=Integer.parseInt(fsx);
				    	Ty=Integer.parseInt(fsy);
				    }catch(NumberFormatException x){
				    	Tx=Ty=0;
				    }
				    fig.Trasladar(Tx, Ty);
				    repaint();
			}});
	   bref.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent arg0) 
		{	
			String fsx=JOptionPane.showInputDialog(
					  "Dame la cantidad a Trasladar en X");
					String fsy=JOptionPane.showInputDialog(
					  "Dame la cantidad a trasladar en Y");
				    int Rx=0,Ry=0;
				    try{
				    	Rx=Integer.parseInt(fsx);
				    	Ry=Integer.parseInt(fsy);
				    }catch(NumberFormatException x){
				    	Rx=Ry=0;
				    }
			fig.ReflexionE(Rx, Ry);
			repaint();
		}
	});
	   cambio.addActionListener(new ActionListener(){
		   public void actionPerformed(ActionEvent d)
		   {
			  if(op==1)
				  op=2;
			  else
				op=1;
			   fig.figura(op);
			   repaint();
		   }
	   });
	   Vent.setVisible(true);
	   Vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   addMouseListener(new manejar(this));
	   addMouseMotionListener(new manejar2(this));
	   addMouseWheelListener(this);
	   //Vent.addKeyListener(obt);
	}
	public void paintComponent(Graphics g){
		super.paintComponent(g); 
		g.setColor(Color.YELLOW);
		g.drawImage(imagen, 0, 0, getSize().width, getSize().height,this);//borra
		fig.dibujar2(g);
		g.drawRect(550, 400, 800, 600);
		fig.mapeo(800, 550, 800, 600, 400, 600,op);
		fig.dibujarM(g,op);
	}
	public static void main(String[] args) {
		new Transformar();
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) 
	{
		int notches=e.getWheelRotation();
		if(notches>0)
		{
			fig.EscalarE(1.1,1.1);
		}
		else
			fig.EscalarE(0.9, 0.9);
		repaint();
	}
}
