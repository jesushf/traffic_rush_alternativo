package Unidad2;
import javax.swing.*;

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
public class figurashape extends Applet implements MouseListener,MouseWheelListener,MouseMotionListener{
	int figura[][]={{350,300},{350,400},{300,400},{400,500},{500,400},{450,400},{450,300},{450,200},{350,200},{350,300}};
	
	Point2D pc1,pc2;
	GeneralPath gp;
	Shape F;
	boolean mover=true;
	public void init(){
		setSize(800,600);
		pc1=new Point2D.Double(500,250);
		pc2=new Point2D.Double(300,250);
		gp=new GeneralPath();
		gp.moveTo(figura[0][0], figura[0][1]);
		for(int i=1;i<=6;i++)
			gp.lineTo(figura[i][0], figura[i][1]);
		gp.curveTo(figura[6][0], figura[6][1], pc1.getX(), pc1.getY(), figura[7][0], figura[7][1]);
		gp.lineTo(figura[8][0], figura[8][1]);
		gp.curveTo(figura[8][0], figura[8][1], pc2.getX(), pc2.getY(), figura[0][0], figura[0][1]);
		F=gp;
		setVisible(true);
		addMouseListener(this);
		addMouseWheelListener(this);
		addMouseMotionListener(this);
	}
	public void paint(Graphics g)
	{
		Graphics2D g2= (Graphics2D)g;
		g2.setPaint(Color.GREEN);
		g2.fill(F);
		g2.setPaint(Color.BLUE);
		g2.setStroke(new BasicStroke(3));
		g2.draw(F);
	
	}
	public void mouseClicked(MouseEvent e) {
		int X=e.getX();
		if(X>F.getBounds2D().getMaxX())
			rotar(5);
		else
			if(X<F.getBounds2D().getMinX())
				rotar(-5);
		int Y=e.getY();
		if(Y<F.getBounds2D().getMinY())
			reflexion(1,-1);
		else
			if(Y>F.getBounds2D().getMinY())
				reflexion(1,-1);
		repaint();
	}
	public void reflexion(double ref1,double ref2)
	{
		AffineTransform at=new AffineTransform();
		at.translate(F.getBounds2D().getCenterX(),F.getBounds2D().getCenterY());
		at.scale(ref1, ref2);
		at.translate(-F.getBounds2D().getCenterX(), -F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	public void rotar(int grados)
	{
		double angR=Math.toRadians((double)grados);
		AffineTransform at=new AffineTransform();
		at.setToRotation(angR,F.getBounds2D().getCenterX(),F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	public void mousePressed(MouseEvent e) {
		Point2D pclick=e.getPoint();
		if(F.contains(pclick))
			mover=true;
		else mover=false;
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int notches=e.getWheelRotation();
		if(notches<0)
			escalar(.9);
		else
			escalar(1.1);
		repaint();
	}
	public void escalar(double S)
	{
		AffineTransform at=new AffineTransform();
		//REGRESAR LA FIGURA A LA POSICION ORIGINAL
		at.translate(F.getBounds2D().getCenterX(), F.getBounds2D().getCenterY());
		//APLICAR LA ESCALA
		at.scale(S,S);
		//mover al origen la figura la original
		at.translate(-F.getBounds2D().getCenterX(), -F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		Point2D pmover=e.getPoint();
		if(mover)
			trasladar(pmover);
		repaint();
	}
	public void trasladar(Point2D pm)
	{
		AffineTransform at=new AffineTransform();
		at.setToTranslation(pm.getX()-F.getBounds2D().getCenterX(),pm.getY()-F.getBounds2D().getCenterY());
		F=at.createTransformedShape(F);
	}
	@Override
	public void mouseMoved(MouseEvent e) {
		
	}
}
