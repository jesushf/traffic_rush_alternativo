package Unidad1;

import java.awt.Color;
import java.awt.Font;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.UIManager;

public class ProgressBar extends JWindow 
{
	private static final long serialVersionUID = 1L;
	JProgressBar barra;
	JLabel fondo,tex;
	URL dir;
	public ProgressBar() 
	{
		LookNimbus();
		this.setLayout(null);
		this.setSize(428,353);
		this.setBounds(325,200,428,353);
		
		//pendiente
		tex=new JLabel("Loading....");
		tex.setHorizontalAlignment(JLabel.CENTER);
		tex.setFont(new Font("Arial",Font.ITALIC,20));
		tex.setForeground(Color.RED);
		
	
		dir=getClass().getResource("/Unidad1/fond1.jpg");
		fondo=new JLabel();
		fondo.setIcon(new ImageIcon(dir));
		fondo.setBounds(0,0,428,353);
		fondo.setOpaque(true);
		barra=new JProgressBar(0,100);
		barra.setStringPainted(true);
		barra.setBounds(100,270,300,25);
		add(barra);
		add(fondo);
		add(tex);
		this.setVisible(true);
	}
	
	
	public void LookNimbus(){
		try
		{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
			}
		catch (Exception e){e.printStackTrace();}
		}
	public void Cargar()
	{
		//barra.setString("Loading.....");
		for (int i=0; i<=barra.getMaximum(); i++) 
		{
			switch(i)
			{
			case 25:
				try
				{
					Thread.sleep(700);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
				break;
			case 50:
				try
				{
					Thread.sleep(500);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
				break;
			case 75:
				try
				{
					Thread.sleep(500);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
				break;
			case 100:
				try
				{
					Thread.sleep(500);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
				barra.setString("100%");
				try
				{
					Thread.sleep(1000);
				}
				catch(InterruptedException e){};
				this.setVisible(false);
				break;
			default:
				try
				{
					Thread.sleep(50);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
				break;
			}
			barra.setValue(i);
			barra.setStringPainted(true);
			if(i==25)
			{
				try
				{
					Thread.sleep(700);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
			}
			if(i==50)
			{
				try
				{
					Thread.sleep(500);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
			}
			if(i==75)
			{
				try
				{
					Thread.sleep(500);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
			}
			if(i==100)
			{
				try
				{
					Thread.sleep(100);
				}
				catch(InterruptedException e){};
				barra.setValue(i);
				barra.setStringPainted(true);
				barra.setString("100%");
				/*
				try
				{
					Thread.sleep(1000);
				}
				catch(InterruptedException e){};
				*/
				this.setVisible(false);
			}
		}
	}
	
	/*
	public static void main(String[] args) 
	{
		//new ProgressBar().Cargar();
		//new programa1();
		//pro.programa1("Laberinto Encantado Version 2.0 GAC");
		
	}
	*/

}
