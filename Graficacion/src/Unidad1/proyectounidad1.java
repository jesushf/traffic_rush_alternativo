package Unidad1;
import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.net.URL;
class teclas extends KeyAdapter{
	proyectounidad1 obj;
	public teclas(proyectounidad1 T){
		obj=T;
	}
	public void keyPressed(KeyEvent e){
		int code=e.getKeyCode();
		String nameKey=e.getKeyText(code);
		obj.teclapresionada=nameKey;
		Dimension d=obj.vent.getSize();//tama�o de la ventana
		switch(code){
		case KeyEvent.VK_RIGHT: 
			if(obj.acaba){
			obj.cx+=obj.vel;
			if(obj.cx>d.width){
				obj.cx=0;
				obj.vueltas++;
				for(int i=0;i<=obj.limite;i++)
					obj.carros[i].vel+=2;
			}
			if((obj.cy<210 || obj.cy>370)&& (obj.cx<265 || obj.cx>480))
				obj.cx-=obj.vel;
			obj.numflecha=0;
			for(int i=0;i<=obj.limite;i++)
				obj.carros[i].colisition(obj.cx, obj.cy, 32,16 );
			}
			break;
		case KeyEvent.VK_LEFT:
			if(obj.acaba){
			obj.cx-=obj.vel;
			if(obj.cx<0)
				obj.cx=d.width;
			if((obj.cy<210 || obj.cy>370)&& (obj.cx<265 || obj.cx>480))
				obj.cx+=obj.vel;
			obj.numflecha=1;
			for(int i=0;i<=obj.limite;i++)
				obj.carros[i].colisition(obj.cx, obj.cy, 32,16 );
			}
			break;
		case KeyEvent.VK_UP: 
			if(obj.acaba){
			obj.cy-=obj.vel;
			if(obj.cy<0)
				obj.cy=d.height;
			if((obj.cy<210 || obj.cy>370)&& (obj.cx<265 || obj.cx>480))
				obj.cy+=obj.vel;
			obj.numflecha=2;
			for(int i=0;i<=obj.limite;i++)
				obj.carros[i].colisition(obj.cx, obj.cy, 32,16 );
			}
			break;
		case KeyEvent.VK_DOWN: 
			if(obj.acaba)
			{
			obj.cy+=obj.vel;
			if(obj.cy>d.height)
			{
				obj.cy=0;
				obj.vueltas++;
				for(int i=0;i<=obj.limite;i++)
					obj.carros[i].vel+=2;
			}
				if((obj.cy<210 || obj.cy>370)&& (obj.cx<265 || obj.cx>480))
					obj.cy-=obj.vel;
			obj.numflecha=3;
			for(int i=0;i<=obj.limite;i++)
				obj.carros[i].colisition(obj.cx, obj.cy, 32,16 );
			}
			break;
		case KeyEvent.VK_A:
			obj.vel+=3;
				if(obj.vel>10)
					obj.vel=1;
				break;
		case KeyEvent.VK_B:
			obj.vel-=3;
				if(obj.vel<1)
					obj.vel=1;
				break;
		case KeyEvent.VK_ENTER:
			obj.acaba=!(obj.acaba);
			break;
	
		}
		obj.repaint();
	}
}
public class proyectounidad1 extends JPanel{
		String teclapresionada="";
		static proyectounidad1 ob;
		boolean acaba=true,empieza=true,otro1=true,otro2=true,otro3=true;
		JFrame vent;
		boolean niveles[]={true,false,false,false};
		JButton reiniciar=new JButton("jugar otra vez");
		int cx=400,cy=300,vel=1,numflecha=0,eliminados=0,carro=0,sheet=0,vueltas=0,nivel=-1,vueltasa=0,limite=0;//0 derecha, 1 izq,2 arr,3 aba
		Carros[] carros,carros1;
		Image imagen;//adaptarse a cualquier tama�o 
		ImageIcon img;//tama�o original de la imagen
		Color pixmap[][]={{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.RED,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.YELLOW},
				{Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED},
				{Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED},
				{Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED},
				{Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.RED,Color.BLACK,Color.RED,Color.RED,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED},
				{Color.WHITE,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED},
				{Color.GRAY,Color.RED,Color.RED,Color.RED,Color.RED,Color.WHITE,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.WHITE,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.BLACK,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED},
				{Color.GRAY,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.RED,Color.BLACK,Color.WHITE,Color.BLACK,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.RED,Color.RED,Color.RED,Color.YELLOW},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE},
				{Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.BLACK,Color.BLACK,Color.BLACK,Color.BLACK,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE,Color.WHITE}};
		public proyectounidad1(){
			vent=new JFrame("Traffic rush");
			
			vent.setSize(800, 600);
			vent.setResizable(false);
			setBackground(Color.LIGHT_GRAY);
			reiniciar.setVisible(false);
			this.add(reiniciar,BorderLayout.SOUTH);
			vent.add(this);
			vent.setVisible(true);
			vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			vent.addKeyListener(new teclas(this));
			carros=new Carros[4];
			carros1=new Carros[carros.length];
				carros[0]=new Carros(this);
				carros[0].start();//iniciar el run
			//cargar las imagenes
			URL ruta=getClass().getResource("/Unidad1/fond1.jpg/");
			//img=new ImageIcon(ruta);
			Toolkit tk=Toolkit.getDefaultToolkit();
			imagen=tk.getImage(ruta);
			reiniciar.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					vent.setVisible(false);
					ob=new proyectounidad1();
				}
			});
		}
		public void paintComponent(Graphics g)
		{
			
			super.paintComponent(g);
			g.setColor(Color.WHITE);
			g.drawImage(imagen, 0, 0, getSize().width, getSize().height,this);
			//img.paintIcon(this, g, 0, 0);
			g.setFont(new Font("Arial",Font.BOLD,20));
			g.drawString("velocidad      "+vel, 10, 20);
			g.drawString("Choques :"+eliminados, 200, 20);
			g.drawString("Vueltas :"+vueltas, 400, 20);
			g.drawString("Nivel :"+nivel, 600, 20);
			g.setFont(new Font("Arial",Font.BOLD,15));
			g.drawString("Tecla A aumenta velocidad", 10, 450);
			g.drawString("Tecla B disminuye velocidad", 10, 500);
			g.drawString("Enter para pausar", 10, 550);
			
			switch(numflecha)
			{
			case 0:
				dibujaPixMap(g,cx,cy);
				break;
			case 1:
				dibujaPixMap(g,cx,cy);
				break;
			case 2:
				dibujaPixMap(g,cx,cy);
				break;
			case 3:
				dibujaPixMap(g,cx,cy);
				break;
			}
			//dibujar flecha
			if(vueltas > vueltasa && Tres())
			empieza=true;
		if(empieza)
		if(Tres() && vueltas!=10)
		{
			nivel++;
			//niveles[nivel]=true;
		}
		for(int i=niveles.length-1;i>=0;i--)
			if(niveles[i])
			{
				limite=i;
				//System.out.println(limite+""+nivel);
				break;
			}
		if(nivel==0)
			for(int i=0;i<=limite;i++)
				if(carros[i].visiblea ||carros[i].visibleab || carros[i].visiblei || carros[i].visibled)
					carros[i].dibujar(g);
		if(nivel==1)
		{
			if(empieza){
			//for(int i=0;i<=limite;i++)
				carros[nivel]=new Carros(this);
				carros[nivel].start();}
			else
				if(!(carros[nivel].visiblea ||carros[nivel].visibleab || carros[nivel].visiblei || carros[nivel].visibled)){
					carros[nivel]=new Carros(this);
					carros[nivel].start();}
			
			for(int i=0;i<=limite;i++)
				if(carros[i].visiblea ||carros[i].visibleab || carros[i].visiblei || carros[i].visibled)
					{
						carros[i].dibujar(g);
					}
		}
		if(nivel==2)
		{
			if(empieza){
			carros[nivel]=new Carros(this);
			carros[nivel].start();
			}
			for(int i=0;i<=limite;i++)
				if(carros[i].visiblea ||carros[i].visibleab || carros[i].visiblei || carros[i].visibled)
					carros[i].dibujar(g);
		}
		if(nivel==3)
		{
			if(empieza ){
			carros[nivel]=new Carros(this);
			carros[nivel].start();
			}
			for(int i=0;i<=limite;i++)
				if(carros[i].visiblea ||carros[i].visibleab || carros[i].visiblei || carros[i].visibled)
					carros[i].dibujar(g);
		}
		empieza=false;
		vueltasa=vueltas;
			if(eliminados>=3)
			{
				g.setFont(new Font("Arial",Font.BOLD,30));
				g.setColor(Color.WHITE);
				g.drawString("GAME OVER", 300, 280);
				Salir();
				acaba=false;
			}
			if(vueltas>=10)
			{
				g.setFont(new Font("Arial",Font.BOLD,30));
				g.setColor(Color.WHITE);
				g.drawString("FELICIDADES GANASTE", 230, 280);
				Salir();
				acaba=false;
			}
			
		}
		public void Salir()
		{
			sheet++;
			if(sheet==3)
				{
					reiniciar.setVisible(true);
					//System.exit(0);
				}
			try{
				Thread.sleep(100);
				}
				catch(InterruptedException d)
				{
					repaint();
				}
		}
		/*public void dibujaBitMap(Graphics g,int x,int y,Color color,short bitmap[])
		{
			g.setColor(color);
			for(int i=0;i<bitmap.length;i++)
				for(int j=0;j<8;j++){
					int mascara=0x80>>>j;
					if((mascara & bitmap[i])!=0)
						g.drawLine(x+j*4,y+i*4,x+j*4,y+i*4);
			repaint();
				}
					
		}*/
		public void dibujaPixMap(Graphics g,int x,int y)
		{
			for(int i=0;i<pixmap.length;i++)
				for(int j=0;j<pixmap[i].length;j++)
				{
					if(pixmap[i][j]!=Color.WHITE){
						g.setColor(pixmap[i][j]);
						g.drawLine(x+j,y+i,x+j,y+i);
					}
					repaint();
				}
		}
		public boolean Tres()
		{
			return vueltas%3==0;
		}
		public static void main(String []x)
		{
			new ProgressBar().Cargar();
			ob=new proyectounidad1();
		}
	}
