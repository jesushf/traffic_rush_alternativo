package Unidad1;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.applet.*;
public class PrimitivaCaracteres extends Applet implements ActionListener {
	char carmap[][]={{' ',' ','*','*','*','*',' ',' '},{' ','*','*',' ',' ','*','*',' ',' '},{'*','*','*',' ',' ','*','*','*'},{'*','*','*',' ',' ','*','*','*'},
	{'*','*','*','*','*','*','*','*'},{'*','*','*','*','*','*','*','*'},{'*','*','*',' ',' ','*','*','*'},{'*','*','*',' ',' ','*','*','*'}};
	Color pixmap[][]={{Color.WHITE,Color.WHITE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE},{Color.WHITE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE},{Color.BLUE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE,Color.BLUE,Color.BLUE,Color.BLUE},{Color.BLUE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE,Color.BLUE,Color.BLUE,Color.BLUE},
			{Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE},{Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE,Color.BLUE},{Color.BLUE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE,Color.BLUE,Color.BLUE,Color.BLUE},{Color.BLUE,Color.BLUE,Color.BLUE,Color.WHITE,Color.WHITE,Color.BLUE,Color.BLUE,Color.BLUE}};
	short bitmap[]={0X3C,0X66,0XE7,0XFF,0XFF,0XE7,0XE7,0XE7};
	Button b1,b2,b3;
	int opc;
	public void init()
	{
		setSize(800,600);
		b1=new Button("Carmap");
		b2=new Button("Pixmap");
		b3=new Button("Bitmap");
		add(b1);add(b2);add(b3);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
	}
	/*public void dibujar(int bm[],int x,int y, Color color)
	{
		for(int i=0;i<bm.length;i++)
		{
			for(int j=0;j<8;j++)
			{
				int mascara=0x80>>j;
				if(((bm[i]) & (mascara))!=0)
					//pintarg.setColor(color);
					//g.drawLine(x+j,y+i,x+j,y+i);
					mascara=mascara;
				
			}
		}
	}*/
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==b1)
			opc=0;
			else
				if(e.getSource()==b2)
					opc=1;
				else
					if(e.getSource()==b3)
						opc=2;
		repaint();
	}
	public void dibujaCarMap(Graphics g,int x,int y,Color color)
	{
		g.setColor(color);
		for(int i=0;i<carmap.length;i++)
			for(int j=0;j<carmap[i].length;j++)
				g.drawString(carmap[i][j]+"",x+j*5,y+i*5);
	}
	public void dibujaPixMap(Graphics g,int x,int y)
	{
		for(int i=0;i<pixmap.length;i++)
			for(int j=0;j<pixmap[i].length;j++)
			{
				g.setColor(pixmap[i][j]);
				g.drawLine(x+j*3,y+i*3,x+j*3,y+i*3);
				
			}
	}
	public void dibujaBitMap(Graphics g,int x,int y,Color color)
	{
		g.setColor(color);
		for(int i=0;i<bitmap.length;i++)
			for(int j=0;j<8;j++){
				int mascara=0x80>>>j;
				if((mascara & bitmap[i])!=0)
					g.drawLine(x+j*4,y+i*4,x+j*4,y+i*4);
			}
				
	}
	public void paint(Graphics g)
	{
		switch(opc){
		case 0:
			dibujaCarMap(g,100,100,Color.BLUE);
			break;
		case 1:
			dibujaPixMap(g,100,100);
			break;
		case 2:
			dibujaBitMap(g,100,100,Color.RED);
			break;
		default:
		}
	}
}
