package Unidad1;
import java.awt.*;
import java.applet.*;
public class Bresenham extends Applet{
	public void linea(int x1,int y1,int x2,int y2,Graphics g)
	{
		int dx=Math.abs(x1-x2),dy=Math.abs(y1-y2);
		int p=2*dy-dx;
		int c1=2*dy;//p>0
		int c2=2*(dy-dx);
		int x,y,xfin;
		if(x1>x2){
			x=x2; y=y2; xfin=x1;
		}
			else{
				x=x1;y=y1; xfin=x2;
			}
		g.drawLine(x, y, x, y);//putPixel
		while(x<xfin)
		{
			x++;
			if(p<0)
				p+=c1;
			else
			{
				y++;
				p+=c2;
			}
			g.drawLine(x, y, x, y);//putPixel
		}
	}
	public void paint(Graphics g)
	{
		Dimension d=getSize();
		g.setColor(Color.BLUE);
		linea(0,0,d.width,d.height,g);
	}
 
}
