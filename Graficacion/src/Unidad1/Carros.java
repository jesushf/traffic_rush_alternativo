package Unidad1;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.net.URL;
import java.util.Random;

import javax.swing.ImageIcon;
public class Carros extends Thread{
	int x,y,ancho,largo,d,i,a,ab,ax,ay,dx,dy,abx,aby,vel=0;
	Color color;
	proyectounidad1 ref;
	boolean visiblei,visibled,visiblea,visibleab,principio=true,ar=true,izq=true,der=true,aba=true;
	Random r=new Random();
	Image img1,img2,img3,img4;
	Toolkit tk;
	URL ruta[]=new URL[4];
	String cad="";
	public Carros(proyectounidad1 T)
	{
		ref=T;
		x=0;
		ay=0;
		dx=ref.vent.getSize().width;
		aby=ref.vent.getSize().height;
		do
			abx=r.nextInt(ref.vent.getSize().width-305);
		while(abx<277);
		do
			dy=r.nextInt(ref.vent.getSize().height-240);
		while(dy<225);
		do
			ax=r.nextInt(ref.vent.getSize().width-305);
		while(ax<277);
		do
		y=r.nextInt(ref.vent.getSize().height-240);
		while(y<225);
		ancho=largo=25;
		visiblei=visibled=visiblea=visibleab=true;
		d=1+r.nextInt(8+vel);
		a=1+r.nextInt(8+vel);
		i=1+r.nextInt(8+vel);
		ab=1+r.nextInt(8+vel);
		
	}
	public void dibujar(Graphics g)
	{
		/*ref.carro++;
		if(ref.carro==5)
			ref.carro=1;*/
		if(der){
		if(principio)
			Generar();
			ruta[0]=getClass().getResource("/Unidad1/"+cad+"der.png");
		}
		tk=Toolkit.getDefaultToolkit();
		img1=tk.getImage(ruta[0]);
		if(visibled){
			g.drawImage(img1, x, y, 32, 16,ref.vent);
			}
		if(aba){
			if(principio)
				Generar();
			ruta[1]=getClass().getResource("/Unidad1/"+cad+"aba.png");
		}
		tk=Toolkit.getDefaultToolkit();
		img2=tk.getImage(ruta[1]);
		if(visibleab){
			g.drawImage(img2, ax, ay, 16, 32,ref.vent);
			}
		if(izq)
		{
			if(principio)
				Generar();
			ruta[2]=getClass().getResource("/Unidad1/"+cad+"izq.png");
		}
		tk=Toolkit.getDefaultToolkit();
		img3=tk.getImage(ruta[2]);
		if(visiblei){
			g.drawImage(img3, dx, dy, 32, 16,ref.vent);
			}
		if(ar)
		{
			if(principio)
				Generar();
			ruta[3]=getClass().getResource("/Unidad1/"+cad+"arr.png");
		}
		tk=Toolkit.getDefaultToolkit();
		img4=tk.getImage(ruta[3]);
		if(visiblea){
			g.drawImage(img4, abx, aby, 16, 32,ref.vent);
			}
			principio=false;
		
	}
	public void Generar()
	{
		int al=r.nextInt(3);
		switch(al)
		{
		case 0:
			cad="trailer";
			break;
		case 1:
			cad="conver";
			break;
		case 2:
			cad="carro";
			break;
		
		}
	}
	public void mover()
	{
		x+=d+vel;
		if(x>ref.vent.getSize().width)
		{
			do
				y=r.nextInt(ref.vent.getSize().height-240);
			while(y<225);
			ancho=largo=25;
			visibled=true;
			d=1+r.nextInt(8);
			x=0;
			der=true;
			ar=izq=aba=false;
			Generar();
		}
		ay+=a+vel;
		if(ay>ref.vent.getSize().height)
		{
			do
				ax=r.nextInt(ref.vent.getSize().width-305);
			while(ax<277);
			ancho=largo=25;
			visiblea=true;
			a=1+r.nextInt(8);
			ay=0;
			aba=true;
			ar=izq=der=false;
			Generar();
		}
		dx-=(i+vel);
		if(dx<=0)
		{
			do
				dy=r.nextInt(ref.vent.getSize().height-240);
			while(dy<225);
			ancho=largo=25;
			visiblei=true;
			i=1+r.nextInt(8);
			dx=ref.vent.getSize().width;
			Generar();
			izq=true;
			ar=aba=der=false;
		}
		aby-=(ab+vel);
		if(aby<=0)
		{
			do
				abx=r.nextInt(ref.vent.getSize().width-305);
			while(abx<277);
			ancho=largo=25;
			visibleab=true;
			ab=1+r.nextInt(8);
			aby=ref.vent.getSize().height;
			ar=true;
			der=izq=aba=false;	
			Generar();
		}
		switch(ref.vueltas)
		{
		case 3:
			ref.niveles[1]=true;
			break;
		case 6:
			ref.niveles[2]=true;
			break;
		case 9:
			ref.niveles[3]=true;
		break;
		}
	}
	public void colisition(int xf,int yf, int anchof,int largof){
		Rectangle2D aread=new Rectangle2D.Double(xf,yf,anchof,largof);
		if(visibled)
		if(aread.intersects(x, y, ancho, largo)){
			ref.eliminados++;
			if(ref.vueltas<3)
			{
				visibled=false;
				//ref.otro1=true;
			}
			else
			{
			visibled=visiblei=visiblea=visibleab=false;
			ar=aba=izq=der=false;
			}
			if(ref.vueltas>2)
			{
				ref.vueltas=ref.vueltas-3;
				ref.nivel--;
				vel-=2;
			}
			else
				ref.vueltas=0;
			
		}
		Rectangle2D areaa=new Rectangle2D.Double(xf,yf,anchof,largof);
		//System.out.println(visibleab+""+visiblei+""+visibled+""+visiblea);
		if(visibleab)
		if(areaa.intersects(ax, ay, 16, 32)){
			ref.eliminados++;
			if(ref.vueltas<3){
				visibleab=false;
				//ref.otro1=true;
			}
			else
			{
				visibled=visiblei=visiblea=visibleab=false;
				ar=aba=izq=der=false;
			}
			if(ref.vueltas>2)
			{
				ref.vueltas=ref.vueltas-3;
				ref.nivel--;
				vel-=2;
			}
			else
				ref.vueltas=0;
		}
		Rectangle2D areai=new Rectangle2D.Double(xf,yf,anchof,largof);
		if(visiblei)
		if(areai.intersects(dx, dy, ancho, largo)){
			ref.eliminados++;
			if(ref.vueltas<3){
				visiblei=false;
				//ref.otro1=true;
			}
			else
			{
				visibled=visiblei=visiblea=visibleab=false;
				ar=aba=izq=der=false;
				}
			if(ref.vueltas>2)
			{
				ref.vueltas=ref.vueltas-3;
				ref.nivel--;
				vel-=2;
			}
			else
				ref.vueltas=0;
		}
		Rectangle2D areaab=new Rectangle2D.Double(xf,yf,anchof,largof);
		if(visiblea)
		if(areaab.intersects(abx, aby, 16, 32)){
			ref.eliminados++;
			if(ref.vueltas<3){
				visiblea=false;
			}
			else
			{
				visibled=visiblei=visiblea=visibleab=false;
				ar=aba=izq=der=false;
				}
			if(ref.vueltas>2)
			{
				ref.vueltas=ref.vueltas-3;
				ref.nivel--;
				vel-=2;
			}
			else
				ref.vueltas=0;
		}
	}
	public void run()
	{
		while((visibled || visiblea || visiblei || visibleab))
		{
			if(ref.acaba)
				mover();
			try{
			Thread.sleep(100);
			}
			catch(InterruptedException d)
			{
				ref.repaint();
			}
		}
	}
}
