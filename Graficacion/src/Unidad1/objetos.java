package Unidad1;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Random;
public class objetos extends Thread{
	int x,y,ancho,largo,velocidad;
	Color color;
	imagenfigura ref;
	boolean visible;
	Random r=new Random();
	public objetos(imagenfigura T)
	{
		ref=T;
		x=0;
		y=r.nextInt(ref.vent.getSize().height-50);
		ancho=largo=25;
		color=new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255));
		visible=true;
		velocidad=1+r.nextInt(5);
	}
	public void dibujar(Graphics g)
	{
		if(visible){
		g.setColor(color);
		g.fillOval(x, y,ancho,largo);}
	}
	public void mover()
	{
		x+=velocidad;
		if(x>ref.vent.getSize().width)
		{
			y=r.nextInt(ref.vent.getSize().height-50);
			ancho=largo=25;
			color=new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255));
			visible=true;
			velocidad=1+r.nextInt(5);
			x=0;
		}
	}
	public void colisition(int xf,int yf, int anchof,int largof){
		Rectangle2D areaf=new Rectangle2D.Double(xf,yf,anchof,largof);
		if(visible)
		if(areaf.intersects(x, y, ancho, largo)){
			ref.eliminados++;
			visible=false;
		}
	}
	public void run()
	{
		while(visible)
		{
			mover();
			try{
			Thread.sleep(100);
			}
			catch(InterruptedException d)
			{
				ref.repaint();
			}
		}
	}
}
