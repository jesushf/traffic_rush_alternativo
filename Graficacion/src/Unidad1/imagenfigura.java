package Unidad1;
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.net.URL;

import javax.swing.*;
//figura en un icono y manipular con teclado y
//agregar algunos objetos aleatoria y se puedan colisionar
class teclado extends KeyAdapter{
	imagenfigura obj;
	public teclado(imagenfigura T){
		obj=T;
	}
	public void keyPressed(KeyEvent e){
		int code=e.getKeyCode();
		String nameKey=e.getKeyText(code);
		obj.teclapresionada=nameKey;
		Dimension d=obj.vent.getSize();//tama�o de la ventana
		switch(code){
		case KeyEvent.VK_RIGHT: obj.cx+=obj.vel;
			if(obj.cx>d.width)
				obj.cx=0;
			obj.numflecha=0;
			for(int i=0;i<obj.rocas.length;i++)
				obj.rocas[i].colisition(obj.cx, obj.cy, 32,32 );
			break;
		case KeyEvent.VK_LEFT: obj.cx-=obj.vel;
			if(obj.cx<0)
				obj.cx=d.width;
			obj.numflecha=1;
			for(int i=0;i<obj.rocas.length;i++)
				obj.rocas[i].colisition(obj.cx, obj.cy, 32,32 );
			break;
		case KeyEvent.VK_UP: obj.cy-=obj.vel;
			if(obj.cy<0)
				obj.cy=d.height;
			obj.numflecha=2;
			for(int i=0;i<obj.rocas.length;i++)
				obj.rocas[i].colisition(obj.cx, obj.cy, 32,32 );
			break;
		case KeyEvent.VK_DOWN: obj.cy+=obj.vel;
			if(obj.cy>d.height)
				obj.cy=0;;
			obj.numflecha=3;
			for(int i=0;i<obj.rocas.length;i++)
				obj.rocas[i].colisition(obj.cx, obj.cy, 32,32 );
			break;
		case KeyEvent.VK_F1:
			obj.vel+=3;
				if(obj.vel>10)
					obj.vel=1;
		}
		obj.repaint();
	}
}
public class imagenfigura extends JPanel{
	short flechad[]={0x08,0x0c,0xfe,0xff,0xfe,0x0c,0x08};
	short flechai[]={0x10,0x30,0x7f,0xff,0x7f,0x30,0x10};
	short flechaarr[]={0x10,0x38,0x7c,0xfe,0x38,0x38,0x38,0x38};
	short flechaaba[]={0x38,0x38,0x38,0x38,0xfe,0x7c,0x38,0x10};
	String teclapresionada="";
	JFrame vent;
	int cx=400,cy=300,vel=1,numflecha=0,eliminados=0;//0 derecha, 1 izq,2 arr,3 aba
	objetos rocas[];
	Image imagen;//adaptarse a cualquier tama�o 
	ImageIcon img;//tama�o original de la imagen
	
	public imagenfigura(){
		vent=new JFrame("Figura");
		
		vent.setSize(800, 600);
		vent.setResizable(false);
		setBackground(Color.LIGHT_GRAY);
		vent.add(this);
		vent.setVisible(true);
		vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		vent.addKeyListener(new teclado(this));
		rocas=new objetos[5];
		for(int i=0;i<rocas.length;i++){
			rocas[i]=new objetos(this);
			rocas[i].start();//iniciar el run
		}
		//cargar las imagenes
		URL ruta=getClass().getResource("/Unidad1/sunset.jpg/");
		//img=new ImageIcon(ruta);
		Toolkit tk=Toolkit.getDefaultToolkit();
		imagen=tk.getImage(ruta);
	}
	public void paintComponent(Graphics g)
	{
		
		super.paintComponent(g);
		g.drawImage(imagen, 0, 0, getSize().width, getSize().height,this);
		//img.paintIcon(this, g, 0, 0);
		g.setFont(new Font("Arial",Font.BOLD,10));
		g.drawString(teclapresionada+"      "+vel, 10, 20);
		g.drawString("Rocas eliminadas :"+eliminados, 80, 20);
		switch(numflecha)
		{
		case 0:
			dibujaBitMap(g,cx,cy,Color.BLUE,flechad);
			break;
		case 1:
			dibujaBitMap(g,cx,cy,Color.BLUE,flechai);
			break;
		case 2:
			dibujaBitMap(g,cx,cy,Color.BLUE,flechaarr);
			break;
		case 3:
			dibujaBitMap(g,cx,cy,Color.BLUE,flechaaba);
			break;
		}
		//dibujar flecha
		for(int i=0;i<rocas.length;i++)
			rocas[i].dibujar(g);
		if(eliminados==rocas.length)
		{
			g.drawString("Felicidades ganaste",300,400);
			//System.exit(0);
		}
	}
	public void dibujaBitMap(Graphics g,int x,int y,Color color,short bitmap[])
	{
		g.setColor(color);
		for(int i=0;i<bitmap.length;i++)
			for(int j=0;j<8;j++){
				int mascara=0x80>>>j;
				if((mascara & bitmap[i])!=0)
					g.drawLine(x+j*4,y+i*4,x+j*4,y+i*4);
				repaint();
			}
				
	}
	public static void main(String []x)
	{
		new imagenfigura();
	}
}
