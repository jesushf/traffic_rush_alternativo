package OpenGL;
import java.awt.BorderLayout;

import javax.swing.*;
public class primer extends JFrame implements GLEventListener{
	GLCanvas lienzo;
	float rot=45.0f;
	public primer()
	{
		super("Primer programa en OPENGL");
		setSize(800,600);
		lienzo=new GLCanvas();
		add(lienzo,BorderLayout.CENTER);
		lienzo.addGlEventListener(this);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public static void main(String []x)
	{
		new primer().rotar();
	}
	public void rotar()
	{
		while(this.isVisible())
		{
			rot+=5.0f;
			try{
				Thread.sleep(100);
			}
			catch(InterruptedException e){}
			if(rot>360.0f)
				rot=0.0f;
			lienzo.repaint();
			
		}
	}
	public void display(GLAutoDrawable g)
	{
		GL gl=g.getGL();
		GLU glu=new GLU();
		gl.glClear(GL.GL_COLOR_BUFFER_BIT|GL.GL_DEPTH_BUFFER_BIT);
		//glu.gluLookAt(0.0,0.0,0.0,-100.0,0.0,0.0,0.0,0.2,0.0);
		gl.glLoadIdentity();
		gl.glTranslatef(0,0,-100);
		gl.glRotatef(rot,0,1,0);
		gl.glColor3f(1,1,0);
		glut.glutWireTeapot(20);
	}
	public void displayChanged(GLAutoDrawable g,boolean a,boolean b)
	{
		
	}
	public void init(GLAutoDrawable g)
	{
		GL gl=g.getGL();
		gl.glClearColor(1,0,0,0);//rgba
		
	}
	public void reshape(GLAutoDrawable g,int x, int y, int ancho, int largo){
		float relacion=(float)ancho/(float)largo;
		GL gl=g.getGL();
		GLU glu=new GLU();
		gl.glMatrixMode(GL.GL_PROJECTION);
		gl.glLoadIdentity();
		glu.gluPerspective(50,relacion,-10,10);
		gl.glMatrixMode(GL.GL_MODELVIEW);
		gl.LoadIdentity();
	}
}
