package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
//java3d
import javax.media.j3d.*;

import java.awt.GraphicsConfiguration;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
public class Apariencia extends JFrame{
	public Apariencia()
	{
		super("programa en java3d que muestra el uso de la apariencia");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		Appearance ap=new Appearance();
		Material solido=new Material();
		ap.setMaterial(solido);
		ColoringAttributes color=new ColoringAttributes(new Color3f(1,1,0),ColoringAttributes.SHADE_GOURAUD);
		ap.setColoringAttributes(color);
		PolygonAttributes pa=new PolygonAttributes(PolygonAttributes.POLYGON_LINE,PolygonAttributes.CULL_NONE,0);
		ap.setPolygonAttributes(pa);
		Sphere S=new Sphere(.5f,50,50,ap);
		//rotacion automatica
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,10000);
		BoundingSphere limite=new BoundingSphere();
		RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
		rot.setSchedulingBounds(limite);
		//
		tg.addChild(rot);
		tg.addChild(S);
		R.addChild(tg);
		return R;
	}
	public static void main(String []x)
	{
		new Apariencia();
	}
}
