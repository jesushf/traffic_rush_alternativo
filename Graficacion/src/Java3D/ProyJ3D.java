package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
//java3d
import javax.media.j3d.*;

import java.awt.*;
import java.awt.event.*;
import java.net.URL;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;
public class ProyJ3D extends JFrame{
	Checkbox cb1,cb2,cb3;
	BranchGroup ramaCont;
	AmbientLight luzAmbiental;
	PointLight foco;
	DirectionalLight luzDireccional,luzDireccional2;
	public ProyJ3D()
	{
		super("programa en java3d con iluminacion");
		cb1=new Checkbox("Ambiental",false);
		cb2=new Checkbox("Direccional",false);
		cb3=new Checkbox("Foco",false);
		Panel gch=new Panel();
		gch.add(cb1);
		gch.add(cb2);
		gch.add(cb3);
		setSize(800,700);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		ramaCont=new BranchGroup();
		ponerLuzAmbiental();
		ponerLuzDireccional();
		ponerFoco();
		ramaCont.addChild(ponerEsfera());
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB,BorderLayout.CENTER);
		add(gch,BorderLayout.NORTH);
		//agregar esferas
		ponerEsfera();
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cb1.addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent arg0)
				{
					if(cb1.getState() && !cb2.getState() && !cb3.getState())
						luzAmbiental.setEnable(true);
					else{
						if(cb3.getState() && !cb2.getState() && !cb1.getState())
							foco.setEnable(true);
						if(cb2.getState() && !cb3.getState() && !cb1.getState()){
							luzDireccional.setEnable(true);
							luzDireccional2.setEnable(true);
						}
						luzAmbiental.setEnable(false);
					}
						
				}});
		cb2.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0)
			{
				if(cb2.getState() && !cb1.getState() && !cb3.getState()){
					luzDireccional.setEnable(true);
					luzDireccional2.setEnable(true);
				}
				else{
					if(cb1.getState() && !cb2.getState() && !cb3.getState())
						luzAmbiental.setEnable(true);
					if(cb3.getState() && !cb2.getState() && !cb1.getState())
						foco.setEnable(true);
					luzDireccional.setEnable(false);
					luzDireccional2.setEnable(false);
				}
			}});
		cb3.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0)
			{
				if(cb3.getState() && !cb2.getState() && !cb1.getState())
					foco.setEnable(true);
				else{
					if(cb1.getState() && !cb2.getState() && !cb3.getState())
						luzAmbiental.setEnable(true);
					if(cb2.getState() && !cb3.getState() && !cb1.getState()){
						luzDireccional.setEnable(true);
						luzDireccional2.setEnable(true);
					}
					foco.setEnable(false);}
			}});
	}
	public void ponerFoco()
	{
		foco=new PointLight(new Color3f(1,1,1),new Point3f(.8f,0,0),new Point3f(1,0,0));
		foco.setEnable(false);
		foco.setCapability(PointLight.ALLOW_STATE_WRITE); 
		//foco.setCapability(PointLight.ALLOW_ATTENUATION_WRITE);
		BoundingSphere espacio2=new BoundingSphere(new Point3d(0,0,0),1);
		foco.setInfluencingBounds(espacio2);
		ramaCont.addChild(foco);
	}
	public void ponerLuzDireccional()
	{
		luzDireccional=new DirectionalLight();
		luzDireccional.setEnable(false);
		luzDireccional.setCapability(DirectionalLight.ALLOW_STATE_WRITE);
		luzDireccional.setColor(new Color3f(0,1,0));
		luzDireccional.setDirection(new Vector3f(-1,0,0));//iluminacion del lado izquierdo
		BoundingSphere espacio=new BoundingSphere(new Point3d(0,0,0),1);
		luzDireccional.setInfluencingBounds(espacio);
		ramaCont.addChild(luzDireccional);
		//otra forma en este constructor ya se deshabilita, se le da el color y el area a iluminar
		 luzDireccional2=new DirectionalLight(false,new Color3f(0,0,1),new Vector3f(0,1,0));
		 luzDireccional2.setCapability(DirectionalLight.ALLOW_STATE_WRITE);
		BoundingSphere espacio2=new BoundingSphere(new Point3d(0,0,0),1);
		luzDireccional2.setInfluencingBounds(espacio2);
		ramaCont.addChild(luzDireccional2);
	}
	public void ponerLuzAmbiental()
	{
		luzAmbiental=new AmbientLight();
		luzAmbiental.setEnable(false);
		luzAmbiental.setCapability(AmbientLight.ALLOW_STATE_WRITE);
		luzAmbiental.setColor(new Color3f(1,1,0));
		BoundingSphere espacio=new BoundingSphere(new Point3d(0,0,0),1);
		luzAmbiental.setInfluencingBounds(espacio);
		ramaCont.addChild(luzAmbiental);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		return R;
	}
	public BranchGroup ponerEsfera()
	{
		BranchGroup R=new BranchGroup();
		//Crear la figura
		Point3d cubo[]=new Point3d[8];
		cubo[0]=new Point3d(-.3,-.3,-.3);
		cubo[1]=new Point3d(.3,-.3,-.3);
		cubo[2]=new Point3d(.3,.3,-.3);
		cubo[3]=new Point3d(-.3,.3,-.3);
		cubo[4]=new Point3d(-.3,-.3,.3);
		cubo[5]=new Point3d(.3,-.3,.3);
		cubo[6]=new Point3d(.3,.3,.3);
		cubo[7]=new Point3d(-.3,.3,.3);
		GeometryInfo gi=new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
		int sec[]={0,1,2,3, 3,2,6,7, 7,6,5,4, 5,4,0,1,  1,2,6,5, 0,4,7,3};
		int caras[]={4,4,4,4,4,4};
		gi.setCoordinates(cubo);
		gi.setCoordinateIndices(sec);
		gi.setStripCounts(caras);
		//Apariencia
		Appearance ap=new Appearance();
		Material mat=new Material();
		ap.setMaterial(mat);
		ColoringAttributes col=new ColoringAttributes(new Color3f(0,0,1),ColoringAttributes.SHADE_GOURAUD);
		ap.setColoringAttributes(col);
		PolygonAttributes pol=new PolygonAttributes(PolygonAttributes.POLYGON_FILL,PolygonAttributes.CULL_NONE,0);
		ap.setPolygonAttributes(pol);
		NormalGenerator ong=new NormalGenerator();
		ong.generateNormals(gi);
		
		TextureAttributes ota=new TextureAttributes();
		ota.setTextureMode(TextureAttributes.MODULATE);
		ap.setTextureAttributes(ota);
		//Texturas
		ImageComponent2D im1=cargarImagen("lap.jpg");
		ImageComponent2D im2=cargarImagen("lap.jpg");
		ImageComponent2D im3=cargarImagen("lap.jpg");
		ImageComponent2D im4=cargarImagen("lap.jpg");
		ImageComponent2D im5=cargarImagen("lap.jpg");
		ImageComponent2D im6=cargarImagen("lap.jpg");
		
		TextureCubeMap cm=new TextureCubeMap(TextureCubeMap.BASE_LEVEL,TextureCubeMap.RGB,im1.getWidth());
		cm.setImage(0, TextureCubeMap.NEGATIVE_X,im1);
		cm.setImage(0, TextureCubeMap.POSITIVE_X,im2);
		cm.setImage(0, TextureCubeMap.NEGATIVE_Y,im3);
		cm.setImage(0, TextureCubeMap.POSITIVE_Y,im4);
		cm.setImage(0, TextureCubeMap.NEGATIVE_Z,im5);
		cm.setImage(0, TextureCubeMap.POSITIVE_Z,im6);
		
		Vector4f planoS=new Vector4f(1,0,0,0);//x
		Vector4f planoT=new Vector4f(0,1,0,0);//y
		Vector4f planoR=new Vector4f(0,0,1,0);//z
		
		TexCoordGeneration tcg=new TexCoordGeneration(TexCoordGeneration.OBJECT_LINEAR,TexCoordGeneration.TEXTURE_COORDINATE_3);
		
		tcg.setPlaneS(planoS);
		tcg.setPlaneT(planoT);
		tcg.setPlaneR(planoR);
		ap.setTexture(cm);
		ap.setTexCoordGeneration(tcg);
		//
		Transform3D t=new Transform3D();
		t.rotX(45);
		TransformGroup tgi=new TransformGroup(t);
		//rotacion automatica
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,10000);
		BoundingSphere limite=new BoundingSphere();
		RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
		rot.setSchedulingBounds(limite);
		//
		Shape3D C=new Shape3D(gi.getGeometryArray(),ap);
		tg.addChild(tgi);
		tg.addChild(rot);
		tgi.addChild(C);
		R.addChild(tg);
		return R;
	}
	public ImageComponent2D cargarImagen(String name)
	{
		ImageComponent2D imagen=null;
		URL ruta=getClass().getResource("/Java3D/imagenes/"+name);
		TextureLoader cargadorImagen=new TextureLoader(ruta,this);
		imagen=cargadorImagen.getImage();
		return imagen;
	}
	public static void main(String []x)
	{
		new ProyJ3D();
	}
}
