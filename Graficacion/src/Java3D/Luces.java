package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;
//java3d
import javax.media.j3d.*;

import java.awt.*;
import java.awt.event.*;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
public class Luces extends JFrame{
	Checkbox cb1,cb2,cb3;
	BranchGroup ramaCont;
	AmbientLight luzAmbiental;
	PointLight foco;
	DirectionalLight luzDireccional,luzDireccional2;
	public Luces()
	{
		super("programa en java3d con iluminacion");
		cb1=new Checkbox("Ambiental",false);
		cb2=new Checkbox("Direccional",false);
		cb3=new Checkbox("Foco",false);
		Panel gch=new Panel();
		gch.add(cb1);
		gch.add(cb2);
		gch.add(cb3);
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		ramaCont=new BranchGroup();
		ponerLuzAmbiental();
		ponerLuzDireccional();
		ponerFoco();
		ramaCont.addChild(ponerEsfera(.25f,new Vector3f(-.5f,.3f,0)));
		ramaCont.addChild(ponerEsfera(.25f,new Vector3f(.5f,.3f,0)));
		ramaCont.addChild(ponerEsfera(.25f,new Vector3f(-.5f,-.3f,0)));
		ramaCont.addChild(ponerEsfera(.25f,new Vector3f(.5f,-.3f,0)));
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB,BorderLayout.CENTER);
		add(gch,BorderLayout.NORTH);
		//agregar esferas
		ponerEsfera(.3f,new Vector3f(-.3f,.3f,0));
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cb1.addItemListener(new ItemListener(){
				public void itemStateChanged(ItemEvent arg0)
				{
					if(cb1.getState())
						luzAmbiental.setEnable(true);
					else
						luzAmbiental.setEnable(false);
				}});
		cb2.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0)
			{
				if(cb2.getState()){
					luzDireccional.setEnable(true);
					luzDireccional2.setEnable(true);
				}
				else{
					luzDireccional.setEnable(false);
					luzDireccional2.setEnable(false);
				}
			}});
		cb3.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent arg0)
			{
				if(cb3.getState())
					foco.setEnable(true);
				else
					foco.setEnable(false);
			}});
	}
	public void ponerFoco()
	{
		foco=new PointLight(new Color3f(1,1,1),new Point3f(.8f,0,0),new Point3f(1,0,0));
		foco.setEnable(false);
		foco.setCapability(PointLight.ALLOW_STATE_WRITE);
		//foco.setCapability(PointLight.ALLOW_ATTENUATION_WRITE);
		BoundingSphere espacio2=new BoundingSphere(new Point3d(0,0,0),1);
		foco.setInfluencingBounds(espacio2);
		ramaCont.addChild(foco);
	}
	public void ponerLuzDireccional()
	{
		luzDireccional=new DirectionalLight();
		luzDireccional.setEnable(false);
		luzDireccional.setCapability(DirectionalLight.ALLOW_STATE_WRITE);
		luzDireccional.setColor(new Color3f(0,1,0));
		luzDireccional.setDirection(new Vector3f(-1,0,0));//iluminacion del lado izquierdo
		BoundingSphere espacio=new BoundingSphere(new Point3d(0,0,0),1);
		luzDireccional.setInfluencingBounds(espacio);
		ramaCont.addChild(luzDireccional);
		//otra forma en este constructor ya se deshabilita, se le da el color y el area a iluminar
		 luzDireccional2=new DirectionalLight(false,new Color3f(0,0,1),new Vector3f(0,1,0));
		 luzDireccional2.setCapability(DirectionalLight.ALLOW_STATE_WRITE);
		BoundingSphere espacio2=new BoundingSphere(new Point3d(0,0,0),1);
		luzDireccional2.setInfluencingBounds(espacio2);
		ramaCont.addChild(luzDireccional2);
	}
	public void ponerLuzAmbiental()
	{
		luzAmbiental=new AmbientLight();
		luzAmbiental.setEnable(false);
		luzAmbiental.setCapability(AmbientLight.ALLOW_STATE_WRITE);
		luzAmbiental.setColor(new Color3f(1,0,0));
		BoundingSphere espacio=new BoundingSphere(new Point3d(0,0,0),1);
		luzAmbiental.setInfluencingBounds(espacio);
		ramaCont.addChild(luzAmbiental);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		return R;
	}
	public BranchGroup ponerEsfera(float tam,Vector3f posicion)
	{
		BranchGroup Rama=new BranchGroup();
		Transform3D tubic=new Transform3D();
		tubic.set(posicion);
		TransformGroup tg=new TransformGroup(tubic);
		Appearance ap=new Appearance();
		Material mat=new Material();
		ap.setMaterial(mat);
		ColoringAttributes ca=new ColoringAttributes(new Color3f(0,0,0.5f),ColoringAttributes.SHADE_GOURAUD);
		ap.setColoringAttributes(ca);
		Sphere S=new Sphere(tam,ap);
		tg.addChild(S);
		Rama.addChild(tg);
		return Rama;
	}
	public static void main(String []x)
	{
		new Luces();
	}
}
