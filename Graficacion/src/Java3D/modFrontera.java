package Java3D;
	import javax.swing.*;
import javax.vecmath.Color3f;
	import javax.vecmath.Point3d;
	import javax.vecmath.Vector3f;
	//java3d
	import javax.media.j3d.*;

	import java.awt.GraphicsConfiguration;
	import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
	public class modFrontera extends JFrame{
		public modFrontera()
		{
			super("programa en java3d TriangleArray");
			setSize(700,500);
			GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
			Canvas3D hojaB=new Canvas3D(cfg);
			BranchGroup ramaCont=escena();
			ramaCont.compile();
			SimpleUniverse su=new SimpleUniverse(hojaB);
			su.getViewingPlatform().setNominalViewingTransform();
			su.addBranchGraph(ramaCont);
			//para ver lo de la rama.
			add(hojaB);
			setVisible(true);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		public BranchGroup escena(){
			BranchGroup R=new BranchGroup();
			Point3d figura[]=new Point3d[12];
			//cara superior
			figura[0]=new Point3d(-.3,.3,.4);//A0
			figura[1]=new Point3d(0,.3,-.4);//B1
			figura[2]=new Point3d(.3,.3,.4);//C2
			//CARA LATERAL DERECHA
			figura[3]=new Point3d(.3,.3,.4);//C2
			figura[4]=new Point3d(0,.3,-.4);//B1
			figura[5]=new Point3d(0,-.3,0);//D3
			//cara lateral izquierda
			figura[6]=new Point3d(0,-.3,0);//D3
			figura[7]=new Point3d(0,.3,-.4);//B1
			figura[8]=new Point3d(-.3,.3,.4);//A0
			//cara trasera
			figura[9]=new Point3d(0,-.3,0);//D3
			figura[10]=new Point3d(-.3,.3,.4);//A0
			figura[11]=new Point3d(.3,.3,.4);//C2
		
			Color3f colores[]=new Color3f[12];
			colores[0]=new Color3f(1,0,0);
			colores[1]=new Color3f(1,0,0);
			colores[2]=new Color3f(1,0,0);
			
			colores[3]=new Color3f(0,1,0);
			colores[4]=new Color3f(0,1,0);
			colores[5]=new Color3f(0,1,0);
			
			colores[6]=new Color3f(0,0,1);
			colores[7]=new Color3f(0,0,1);
			colores[8]=new Color3f(0,0,1);
			
			colores[9]=new Color3f(.5f,.5f,.5f);
			colores[10]=new Color3f(.5f,.5f,.5f);
			colores[11]=new Color3f(.5f,.5f,.5f);
			TriangleArray ta=new TriangleArray(figura.length,GeometryArray.COORDINATES|GeometryArray.COLOR_3);
			ta.setCoordinates(0,figura);
			ta.setColors(0,colores);
			Shape3D S=new Shape3D(ta);
			//inclinacion
			/*TransformGroup tra=new TransformGroup();
			Transform3D rox=new Transform3D();
			roty.rotY(30);
			rotx.mul(roty);
			TransformGroup tra=new TransformGroup(rotx);*/
			//rotacion automatica
			TransformGroup tg=new TransformGroup();
			tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
			Alpha tiempo=new Alpha(-1,5000);
			BoundingSphere limite=new BoundingSphere();
			RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
			rot.setSchedulingBounds(limite);
			//
			tg.addChild(S);
			tg.addChild(rot);
			R.addChild(tg);
			
			
			return R;
		}
		public static void main(String []x)
		{
			new modFrontera();
		}
	}

