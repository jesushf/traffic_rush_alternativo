package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
//java3d
import javax.media.j3d.*;

import java.awt.GraphicsConfiguration;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
public class modGI extends JFrame{
	public modGI()
	{
		super("programa en java3d para un geometryInfo");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		Point3d fig[]=new Point3d[4];
		fig[0]=new Point3d(-.3,.3,.4);//A
		fig[1]=new Point3d(0,.3,-.4);//B
		fig[2]=new Point3d(.3,.3,.4);//C
		fig[3]=new Point3d(0,-.3,0);//D
		GeometryInfo GI=new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
		int secCaras[]={0,1,2, 2,1,3, 3,1,0, 3,0,2};
		GI.setCoordinates(fig);
		GI.setCoordinateIndices(secCaras);
		Color3f colores[]=new Color3f[4];
		colores[0]=new Color3f(1,0,0);
		colores[1]=new Color3f(0,1,0);
		colores[2]=new Color3f(0,0,1);
		colores[3]=new Color3f(1,1,0);
		int secColores[]={0,0,0, 0,1,2, 1,1,1, 3,3,3};
		GI.setColors(colores);
		GI.setColorIndices(secColores);
		int tiras[]={3,3,3,3};
		GI.setStripCounts(tiras);
		Shape3D S=new Shape3D(GI.getGeometryArray());
		//rotacion automatica
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,5000);
		BoundingSphere limite=new BoundingSphere();
		RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
		rot.setSchedulingBounds(limite);
		//
		//fondo solido
		Background bg=new Background(0.9f,0.9f,0.9f);
		bg.setApplicationBounds(limite);
		tg.addChild(bg);
		tg.addChild(rot);
		tg.addChild(S);
		R.addChild(tg);
		return R;
	}
	public static void main(String []x)
	{
		new modGI();
	}
}
