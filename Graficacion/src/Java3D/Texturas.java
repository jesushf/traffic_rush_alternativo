package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
//java3d
import javax.media.j3d.*;
import java.util.*;
import java.net.*;
import java.awt.GraphicsConfiguration;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;
public class Texturas extends JFrame{
	public Texturas()
	{
		super("Texturas en java3d");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		//Apariencia
		Appearance ap=new Appearance();
		Material mat=new Material();
		ap.setMaterial(mat);
		ColoringAttributes col=new ColoringAttributes(new Color3f(0,0,1),ColoringAttributes.SHADE_GOURAUD);
		
		ap.setColoringAttributes(col);
		PolygonAttributes pol=new PolygonAttributes(PolygonAttributes.POLYGON_FILL,PolygonAttributes.CULL_NONE,0);
		//fondo
		ImageComponent2D imagen=cargarImagen("lienzo2.jpg");
		Background fondo=new Background(imagen);
		//tama�o de la imagen
		BoundingSphere tama=new BoundingSphere();
		fondo.setApplicationBounds(tama);
		//textura
		Texture2D text=null;
		int r=new Random().nextInt(4);
		if(r==0)
			r=1;
		switch(r)
		{
		case 1:
			text=cargarTextura("sunset.jpg");
			break;
		case 2:
			text=cargarTextura("lienzo1.jpg");
			break;
		case 3:
			text=cargarTextura("butterfly.jpg");
			break;
		}
		
		ap.setTexture(text);
		//rotacion automatica
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,10000);
		BoundingSphere limite=new BoundingSphere();
		RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
		rot.setSchedulingBounds(limite);
		//
		Sphere S=new Sphere(.5f,50,50,ap);
		R.addChild(fondo);
		R.addChild(tg);
		tg.addChild(rot);
		tg.addChild(S);
		return R;
	}
	public Texture2D cargarTextura(String textura){
		Texture2D reg=null;
		ImageComponent2D imagen=cargarImagen(textura);
		reg=new Texture2D(Texture2D.BASE_LEVEL,Texture2D.RGB,imagen.getWidth(),imagen.getHeight());
		reg.setImage(0, imagen);
		reg.setEnable(true);
		return reg;
	}
	public ImageComponent2D cargarImagen(String name){
		ImageComponent2D imagen=null;
		URL ruta=getClass().getResource("/Java3D/imagenes/"+name);
		TextureLoader cargadorImagen=new TextureLoader(ruta,this);
		imagen=cargadorImagen.getImage();
		return imagen;
	}
	public static void main(String []x)
	{
		new Texturas();
	}
}
