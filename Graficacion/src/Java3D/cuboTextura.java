package Java3D;

import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.net.URL;
import javax.swing.*;
import javax.media.j3d.*;
import javax.vecmath.*;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;
//Puntos de la piramide
public class cuboTextura extends JFrame
{	
	public cuboTextura()
	{
		super("Cubo con Texturas diferentes en Java3D");
		
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=Escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		add(hojaB);
		setSize(700,500);
		setBackground(Color.LIGHT_GRAY);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public ImageComponent2D cargarImagen(String name)
	{
		ImageComponent2D imagen=null;
		URL ruta=getClass().getResource("/Java3D/imagenes/"+name);
		TextureLoader cargadorImagen=new TextureLoader(ruta,this);
		imagen=cargadorImagen.getImage();
		return imagen;
	}
	
	public BranchGroup Escena()
	{
		BranchGroup rama=new BranchGroup();
		Point3d piramide[]=new Point3d[5];
		piramide[0]=new Point3d(-.4,-0.5,-.4);
		piramide[1]=new Point3d(.4,-0.5,-.4);
		piramide[2]=new Point3d(-.4,-.5,.4);
		piramide[3]=new Point3d(.4,-.5,.4);
		piramide[4]=new Point3d(0,.5,0);
		GeometryInfo gi=new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
		int sec[]={0,1,2,3, 3,4,2,3, 3,4,1,3, 0,1,4,0, 0,4,2,0}; 
		int caras[]={4,4,4,4,4};
		gi.setCoordinates(piramide);
		gi.setCoordinateIndices(sec);
		gi.setStripCounts(caras);
		
		//Apariencia
		Appearance ap=new Appearance();
		Material mat=new Material();
		ap.setMaterial(mat);
		ColoringAttributes col=new ColoringAttributes(new Color3f(0,0,1), ColoringAttributes.SHADE_GOURAUD);
		ap.setColoringAttributes(col);
		PolygonAttributes pol=new PolygonAttributes(PolygonAttributes.POLYGON_FILL,PolygonAttributes.CULL_NONE,0);
		ap.setPolygonAttributes(pol);
		
		//Texturas
		ImageComponent2D im1=cargarImagen("1.png");
		ImageComponent2D im2=cargarImagen("2.png");
		ImageComponent2D im3=cargarImagen("3.png");
		ImageComponent2D im4=cargarImagen("4.png");
		ImageComponent2D im5=cargarImagen("5.png");
		
		TextureCubeMap cm=new TextureCubeMap(TextureCubeMap.BASE_LEVEL,TextureCubeMap.RGB,im1.getWidth());
		cm.setImage(0, TextureCubeMap.NEGATIVE_X,im1);
		cm.setImage(0, TextureCubeMap.POSITIVE_X,im2);
		cm.setImage(0, TextureCubeMap.NEGATIVE_Y,im3);
		cm.setImage(0, TextureCubeMap.POSITIVE_Y,im4);
		cm.setImage(0, TextureCubeMap.NEGATIVE_Z,im5);
		
		Vector4f planoS=new Vector4f(1,0,0,0); //x
		Vector4f planoT=new Vector4f(0,1,0,0); //y
		Vector4f planoR=new Vector4f(0,0,1,0); //z
		
		TexCoordGeneration tcg=new TexCoordGeneration(TexCoordGeneration.OBJECT_LINEAR,TexCoordGeneration.TEXTURE_COORDINATE_3);
		tcg.setPlaneS(planoS);
		tcg.setPlaneT(planoT);
		tcg.setPlaneR(planoR);
		
		ap.setTexture(cm);
		ap.setTexCoordGeneration(tcg);
		
		//Inclinación
		Transform3D t=new Transform3D();
		t.rotX(40);
		TransformGroup incli=new TransformGroup(t);
		
		//rotación automática
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,5000);
		BoundingSphere limite=new BoundingSphere(); //1
		RotationInterpolator rot=new RotationInterpolator(tiempo, tg);
		rot.setSchedulingBounds(limite);
		
		Shape3D C=new Shape3D(gi.getGeometryArray(),ap);
		
		tg.addChild(incli);
		tg.addChild(rot);
		incli.addChild(C);
		rama.addChild(tg);
		
		return rama;
	}
	
	public static void main(String[] args)
	{
		new cuboTextura();
	}
}