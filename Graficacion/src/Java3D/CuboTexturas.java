package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;
//java3d
import javax.media.j3d.*;

import java.awt.GraphicsConfiguration;
import java.net.URL;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.image.TextureLoader;
public class CuboTexturas extends JFrame{
	public CuboTexturas()
	{
		super("Cubo con texturas diferentes en java3d");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		//Crear la figura
		Point3d cubo[]=new Point3d[8];
		cubo[0]=new Point3d(-.3,-.3,-.3);
		cubo[1]=new Point3d(.3,-.3,-.3);
		cubo[2]=new Point3d(.3,.3,-.3);
		cubo[3]=new Point3d(-.3,.3,-.3);
		cubo[4]=new Point3d(-.3,-.3,.3);
		cubo[5]=new Point3d(.3,-.3,.3);
		cubo[6]=new Point3d(.3,.3,.3);
		cubo[7]=new Point3d(-.3,.3,.3);
		GeometryInfo gi=new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
		int sec[]={0,1,2,3, 3,2,6,7, 7,6,5,4, 5,4,0,1,  1,2,6,5, 0,4,7,3};
		int caras[]={4,4,4,4,4,4};
		gi.setCoordinates(cubo);
		gi.setCoordinateIndices(sec);
		gi.setStripCounts(caras);
		//Apariencia
		Appearance ap=new Appearance();
		Material mat=new Material();
		ap.setMaterial(mat);
		ColoringAttributes col=new ColoringAttributes(new Color3f(0,0,1),ColoringAttributes.SHADE_GOURAUD);
		ap.setColoringAttributes(col);
		PolygonAttributes pol=new PolygonAttributes(PolygonAttributes.POLYGON_FILL,PolygonAttributes.CULL_NONE,0);
		ap.setPolygonAttributes(pol);
		//Texturas
		ImageComponent2D im1=cargarImagen("1.png");
		ImageComponent2D im2=cargarImagen("2.png");
		ImageComponent2D im3=cargarImagen("3.png");
		ImageComponent2D im4=cargarImagen("4.png");
		ImageComponent2D im5=cargarImagen("5.png");
		ImageComponent2D im6=cargarImagen("6.png");
		
		TextureCubeMap cm=new TextureCubeMap(TextureCubeMap.BASE_LEVEL,TextureCubeMap.RGB,im1.getWidth());
		cm.setImage(0, TextureCubeMap.NEGATIVE_X,im1);
		cm.setImage(0, TextureCubeMap.POSITIVE_X,im2);
		cm.setImage(0, TextureCubeMap.NEGATIVE_Y,im3);
		cm.setImage(0, TextureCubeMap.POSITIVE_Y,im4);
		cm.setImage(0, TextureCubeMap.NEGATIVE_Z,im5);
		cm.setImage(0, TextureCubeMap.POSITIVE_Z,im6);
		
		Vector4f planoS=new Vector4f(1,0,0,0);//x
		Vector4f planoT=new Vector4f(0,1,0,0);//y
		Vector4f planoR=new Vector4f(0,0,1,0);//z
		
		TexCoordGeneration tcg=new TexCoordGeneration(TexCoordGeneration.OBJECT_LINEAR,TexCoordGeneration.TEXTURE_COORDINATE_3);
		
		tcg.setPlaneS(planoS);
		tcg.setPlaneT(planoT);
		tcg.setPlaneR(planoR);
		ap.setTexture(cm);
		ap.setTexCoordGeneration(tcg);
		//
		Transform3D t=new Transform3D();
		t.rotX(45);
		TransformGroup tgi=new TransformGroup(t);
		//rotacion automatica
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,10000);
		BoundingSphere limite=new BoundingSphere();
		RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
		rot.setSchedulingBounds(limite);
		//
		Shape3D C=new Shape3D(gi.getGeometryArray(),ap);
		tg.addChild(tgi);
		tg.addChild(rot);
		tgi.addChild(C);
		R.addChild(tg);
		return R;
	}
	public ImageComponent2D cargarImagen(String name){
		ImageComponent2D imagen=null;
		URL ruta=getClass().getResource("/Java3D/imagenes/"+name);
		TextureLoader cargadorImagen=new TextureLoader(ruta,this);
		imagen=cargadorImagen.getImage();
		return imagen;
	}
	/*//metodo para poner la imagen de la tierra
	 public Texture2D cargarTextura(String textura)
	{
		Texture2D reg=null;
		ImageComponent2D imagen=cargarImagen(textura);
		reg=new Texture2D(Texture2D.BASE_LEVEL,Texture2D.RGB,imagen.getWidth(),imagen.getHeight());
		reg.setImage(0,imagen);
		reg.setEnable(true);
		return reg;
	}*/
	public static void main(String []x)
	{
		new CuboTexturas();
	}
}
