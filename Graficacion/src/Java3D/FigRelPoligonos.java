package Java3D;

import javax.swing.*;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
//java3d
import javax.media.j3d.*;
import java.awt.GraphicsConfiguration;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
public class FigRelPoligonos extends JFrame{
	public FigRelPoligonos()
	{
		super("Primer programa en java3d");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		Point3d fig[]=new Point3d[10];
		fig[0]=new Point3d(-.3,-.3,0);//A
		fig[1]=new Point3d(0,-.3,0);//b
		fig[2]=new Point3d(0,0,0);//c
		fig[3]=new Point3d(.3,0,0);//d
		fig[4]=new Point3d(.3,.3,0);//e
		fig[5]=new Point3d(0,.3,0);//f
		fig[6]=new Point3d(0,.2,0);//g
		fig[7]=new Point3d(-.1,.2,0);//h
		fig[8]=new Point3d(-.1,.3,0);//i
		fig[9]=new Point3d(-.3,.3,0);//j
		GeometryInfo gi=new GeometryInfo(GeometryInfo.POLYGON_ARRAY);
		int secCara[]={0,1,2,3,4,5,6,7,8,9};
		gi.setCoordinates(fig);
		gi.setCoordinateIndices(secCara);
		Color3f color[]=new Color3f[2];
		color[0]=new Color3f(1,0,0);
		color[1]=new Color3f(0,1,0);
		gi.setColors(color);
		int secColor[]={0,0,0,0,0,0,0,0,0,0};
		int tiras[]={10};
		gi.setStripCounts(tiras);
		gi.setColorIndices(secColor);
		//Apariencia
		Appearance ap=new Appearance();
		Material defaultMaterial=new Material();
		ap.setMaterial(defaultMaterial);
		ColoringAttributes ca=new ColoringAttributes(new Color3f(1,0,0),ColoringAttributes.SHADE_GOURAUD);
		ap.setColoringAttributes(ca);
		PolygonAttributes pa=new PolygonAttributes(PolygonAttributes.POLYGON_LINE,PolygonAttributes.CULL_NONE,0);
		ap.setPolygonAttributes(pa);
		Shape3D S=new Shape3D(gi.getGeometryArray(),ap);
		R.addChild(S);
		return R;
		
	}
	public static void main(String []x)
	{
		new FigRelPoligonos();
	}
}
