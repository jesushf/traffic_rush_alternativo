package Java3D;

import javax.swing.*;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;
//java3d
import javax.media.j3d.*;
import java.awt.GraphicsConfiguration;
import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;
public class uno extends JFrame{
	public uno()
	{
		super("Primer programa en java3d");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup R=new BranchGroup();
		Transform3D trans=new Transform3D();
		trans.rotX(45);
		Transform3D trans1=new Transform3D();
		trans1.rotY(45);
		Vector3f ubicacion=new Vector3f(.5f,.3f,0f);
		trans.mul(trans1);//rotx 30 grados y roty 30 grados
		//trans.set(ubicacion);//trasladar la figura
		//trans.setScale(1.2);//Escalar la figura
		Alpha tiempo=new Alpha(-1,10000);
		BoundingSphere limite=new BoundingSphere(new Point3d(0,0,0),0.5);
		
		TransformGroup tg=new TransformGroup(trans);
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		RotationInterpolator rotAuto=new RotationInterpolator(tiempo,tg);
		rotAuto.setSchedulingBounds(limite);
		R.addChild(tg);
		R.addChild(new ColorCube(.2));
		R.addChild(rotAuto);
		return R;
	}
	public static void main(String []x)
	{
		new uno();
	}
}
