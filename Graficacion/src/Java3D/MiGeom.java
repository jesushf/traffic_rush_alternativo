package Java3D;

import java.awt.GraphicsConfiguration;

import javax.swing.*;
import javax.vecmath.Point3d;
//java3d
import javax.media.j3d.*;

import com.sun.j3d.utils.universe.*;
import com.sun.j3d.utils.geometry.*;

public class MiGeom extends JFrame{
	public MiGeom()
	{
		super("Primer programa en java3d");
		setSize(700,500);
		GraphicsConfiguration cfg=SimpleUniverse.getPreferredConfiguration();
		Canvas3D hojaB=new Canvas3D(cfg);
		BranchGroup ramaCont=escena();
		ramaCont.compile();
		SimpleUniverse su=new SimpleUniverse(hojaB);
		su.getViewingPlatform().setNominalViewingTransform();
		su.addBranchGraph(ramaCont);
		//para ver lo de la rama.
		add(hojaB);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public BranchGroup escena(){
		BranchGroup rama=new BranchGroup();
		Point3d lapiz[]=new Point3d[10];
		lapiz[0]=new Point3d(-.3,-.3,-.2);//A
		lapiz[1]=new Point3d(0,-.5,-.2);//B
		lapiz[2]=new Point3d(.3,-.3,-.2);//C
		lapiz[3]=new Point3d(.3,.3,-.2);//D
		lapiz[4]=new Point3d(-.3,.3,-.2);//E
		lapiz[5]=new Point3d(-.3,-.3,.2);//A'
		lapiz[6]=new Point3d(0,-.5,.2);//B'
		lapiz[7]=new Point3d(.3,-.3,.2);//C'
		lapiz[8]=new Point3d(.3,.3,.2);//D'
		lapiz[9]=new Point3d(-.3,.3,.2);//E'
		int sec[]={0,1,1,2,2,3,3,4,0,5,6,6,7,7,8,8,9,9,5,0,5,1,6,2,7,3,8,4,9};
		IndexedLineArray figura=new IndexedLineArray(lapiz.length,GeometryArray.COORDINATES,sec.length);
		figura.setCoordinates(0,lapiz);
		figura.setCoordinateIndices(0,sec);
		Shape3D S=new Shape3D(figura);
		//rotacion automatica
		TransformGroup tg=new TransformGroup();
		tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		Alpha tiempo=new Alpha(-1,5000);
		BoundingSphere limite=new BoundingSphere();
		RotationInterpolator rot=new RotationInterpolator(tiempo,tg);
		rot.setSchedulingBounds(limite);
		//
		Sphere es=new Sphere(10);
		tg.addChild(S);
		tg.addChild(rot);
		rama.addChild(tg);
		return rama;
	}
	public static void main(String []x)
	{
		new MiGeom();
	}
}
